module.exports = {
  alertConstants: {
    SUCCESS: 'ALERT_SUCCESS',
    ERROR: 'ALERT_ERROR',
    CLEAR: 'ALERT_CLEAR',
  },
  userConstants: {
    REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',

    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',

    LOGOUT: 'USERS_LOGOUT',
  },
  postConstants: {
    FETCH_POST: 'FETCH_POST',
    ADD_POST: 'ADD_POST',
    DELETE_POST: 'DELETE_POST',
    FETCH_POST_BY_ID_COMPANY: 'FETCH_POST_BY_ID_COMPANY',
    FETCH_POST_BY_ID_COMPANY_FAILED: 'FETCH_POST_BY_ID_COMPANY_FAILED',
    GET_POST_BY_ID: 'GET_POST_BY_ID',
    UPDATE_POST: 'UPDATE_POST',
    SET_INPUT_UPDATE: 'SET_INPUT_UPDATE',
    GET_COMMENT_BY_ID: 'GET_COMMENT_BY_ID',
    SET_COMMENT: 'SET_COMMENT',
    GET_JOB_POSITION: 'GET_JOB_POSITION',
    GET_CHILD_PRO_SKILL: 'GET_CHILD_PRO_SKILL',
  },
  cityConstants: {
    FETCH_PROVINCE: 'FETCH_PROVINCE',
    FETCH_DISTRICT: 'FETCH_DISTRICT',
    FETCH_WARD: 'FETCH_WARD',
  },
  profileConstants: {
    FETCH_PROFILE: 'FETCH_PROFILE',
    UPDATE_PROFILE: 'UPDATE_PROFILE',
    UPLOAD_AVATAR: 'UPLOAD_AVATAR',
  },
  JOB_CATEGORY: 'JOB_CATEGORY',
  CANDIDATE: {
    FETCH_CANDIDATE_SUCCESS: 'FETCH_CANDIDATE_SUCCESS',
    FETCH_CANDIDATE_FAILED: 'FETCH_CANDIDATE_FAILED',
    GET_PROFILE_CANDIDATE: 'GET_PROFILE_CANDIDATE',
  },
};
