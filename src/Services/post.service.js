import { apiUrlPostService, apiUrlCityService } from '../Config';
import callApi from '../Utils/apiCaller';
import authHeader from '../Helpers/AuthHeader';

export const createPost = (data) => {
  return callApi(apiUrlPostService, 'post', 'POST', authHeader(), data);
};

export const fetchPostByidCompany = (
  companyId,
  postType,
  offset,
  jobType,
  jobCategory,
) => {
  const q = {
    companyId,
    postType,
    jobType,
    jobCategory,
  };
  return callApi(
    apiUrlPostService,
    `post-for-company-admin?q=${JSON.stringify(
      q,
    )}&offset=${offset}&limit=10&getTotal=true&reverse=true`,
    'GET',
    authHeader(),
    null,
  );
};

export const deletePostItem = (postId) => {
  return callApi(
    apiUrlPostService,
    `post?q={"postId":"${postId}"}`,
    'DELETE',
    authHeader(),
    null,
  );
};

export const getJobCategory = () =>
  callApi(
    apiUrlCityService,
    'info/professional-skill?q={}',
    'GET',
    authHeader(),
    null,
  );

export const getPostById = (id) =>
  callApi(
    apiUrlPostService,
    `post-for-company-admin?q={"postId":"${id}"}`,
    'GET',
    authHeader(),
    null,
  );

export const updatePost = (data) =>
  callApi(apiUrlPostService, 'post', 'PUT', authHeader(), data);

export const getCommentByPostId = (id) =>
  callApi(
    apiUrlPostService,
    `post/comment?q={"postId": "${id}"}`,
    'GET',
    authHeader(),
    null,
  );

export const setComment = (data) =>
  callApi(apiUrlPostService, 'post/comment', 'POST', authHeader(), data);

export const getJobPositon = () =>
  callApi(
    apiUrlCityService,
    'info/job-position?q={"name":""}',
    'GET',
    authHeader(),
    null,
  );

export const getChildProSkill = () =>
  callApi(
    apiUrlCityService,
    `info/child-professional-skill?q={}`,
    'GET',
    authHeader(),
    null,
  );
