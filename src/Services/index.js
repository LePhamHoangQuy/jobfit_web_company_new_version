/* eslint-disable camelcase */
import { apiUrlCityService } from '../Config';
import callApi from '../Utils/apiCaller';
import authHeader from '../Helpers/AuthHeader';

export const login = (email, password) =>
  callApi(apiUrlCityService, `auth/customer/login`, 'POST', null, {
    email,
    password,
  });

export const getTransactionLog = (
  accNumber,
  page,
  isReceiver,
  isSender,
  isRemind,
  isBeRemind,
) => {
  const query = {
    isReceiver,
    isSender,
    isRemind,
    isBeRemind,
  };
  return callApi(
    apiUrlCityService,
    `employee/history/${accNumber}?q=${JSON.stringify(
      query,
    )}&page=${page}&per_page=10`,
    'GET',
    authHeader(),
    null,
  );
};

export const findCustomer = (account_number) =>
  callApi(apiUrlCityService, `debit/verify-contact`, 'POST', authHeader(), {
    account_number,
  });

export const createDebit = (id, amount, message) =>
  callApi(apiUrlCityService, 'debit', 'POST', authHeader(), {
    reminder_id: id,
    amount,
    message,
  });

export const createContact = (reminder_name, account_number) =>
  callApi(apiUrlCityService, `customer/create-contact`, 'PUT', authHeader(), {
    reminder_name,
    account_number,
  });

export const getContactList = () =>
  callApi(
    apiUrlCityService,
    `customer/list-contacts`,
    'GET',
    authHeader(),
    null,
  );

export const deleteContact = (accNumber) =>
  callApi(
    apiUrlCityService,
    `customer/list-contacts/${accNumber}`,
    'DELETE',
    authHeader(),
    null,
  );

export const getListDebit = () =>
  callApi(apiUrlCityService, `customer/debits`, 'GET', authHeader(), null);

export const getProfile = (account_number) =>
  callApi(apiUrlCityService, `employee/verify-customer`, 'POST', authHeader(), {
    account_number,
  });
