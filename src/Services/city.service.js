import { apiUrlCityService, apiUrlNoti } from '../Config';
import callApi from '../Utils/apiCaller';
import authHeader from '../Helpers/AuthHeader';

export const getAllProvince = () => {
  return callApi(apiUrlCityService, 'info/province?q={}', 'GET', null, null);
};

export const getDistrictByProvinceId = (provinceId) => {
  return callApi(
    apiUrlCityService,
    `info/district?q={"provinceId":${provinceId}}`,
    'GET',
    null,
    null,
  );
};

export const getWardByDistrictId = (districtId) => {
  return callApi(
    apiUrlCityService,
    `info/ward?q={"districtId":${districtId}}`,
    'GET',
    null,
    null,
  );
};

export const getNotiList = () =>
  callApi(apiUrlNoti, 'api/v1/company/notis', 'GET', authHeader(), null);

export const makeSeenNoti = (id) =>
  callApi(
    apiUrlNoti,
    `api/v1/employee/notis/${id}/seen`,
    'GET',
    authHeader(),
    null,
  );
