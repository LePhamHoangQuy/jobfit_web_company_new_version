import { apiUrlIAM, apiUrlImage } from '../Config';
import callApi from '../Utils/apiCaller';
import authHeader from '../Helpers/AuthHeader';

export const register = (user) => {
  return callApi(apiUrlIAM, 'auth/register', 'POST', null, user);
};

export const login = (email, password, roleName = 'COMPANY') => {
  return callApi(apiUrlIAM, 'auth/login', 'POST', null, {
    email,
    password,
    roleName,
  });
};

export const getCompanyProfile = () => {
  return callApi(apiUrlIAM, `company/me`, 'GET', authHeader(), null);
};

export const updateCompanyProfile = (data) => {
  return callApi(
    apiUrlIAM,
    `company/update_profile`,
    'PUT',
    authHeader(),
    data,
  );
};

export const updateAvatar = (image) =>
  callApi(
    apiUrlImage,
    'image-upload',
    'POST',
    { ...authHeader(), 'Content-Type': 'multipart/form-data' },
    image,
  );

export const updatePassword = (currentPassword, newPassword) =>
  callApi(apiUrlIAM, `auth/update_password`, 'PUT', authHeader(), {
    currentPassword,
    newPassword,
  });

export const forgetPassword = (email, roleName) =>
  callApi(apiUrlIAM, `auth/forgot_password`, 'POST', authHeader(), {
    email,
    roleName,
  });
