import { apiUrlPostService, apiUrlIAM } from '../Config';
import callApi from '../Utils/apiCaller';
import authHeader from '../Helpers/AuthHeader';

export const fetchCandidate = (companyId, offset) =>
  callApi(
    apiUrlPostService,
    `post/apply-by-employee?q={"companyId": "${companyId}"}&offset=${offset}&limit=10&getTotal=true&reverse=true`,
    'GET',
    authHeader(),
    { companyId },
  );

export const getProfileCandidate = (id) =>
  callApi(apiUrlIAM, `company/employee/${id}`, 'GET', authHeader(), null);

export const getCandidateFollow = () =>
  callApi(apiUrlIAM, 'company/followers', 'GET', authHeader(), null);

export const rejectCV = (userId, postId) =>
  callApi(apiUrlPostService, 'post/reject-cv', 'PUT', authHeader(), {
    userId,
    postId,
  });

export const approveCV = (userId, postId) =>
  callApi(apiUrlPostService, 'post/approve-cv', 'PUT', authHeader(), {
    userId,
    postId,
  });
