/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

import { Topbar } from './components';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 64,
    height: '100%',
  },
  content: {
    height: '100%',
  },
  topbar: {
    height: 90,
    background: `linear-gradient(90deg, rgba(9, 31, 38, 1) 0%, rgba(60, 130, 147, 1) 43%, rgba(76, 103, 159, 1) 67%, rgba(57, 96, 125, 1) 100%, rgba(43, 136, 136, 1) 100%, rgba(84, 64, 24, 1) 100%, rgba(0, 212, 255, 1) 100%, rgba(42, 49, 224, 1) 100%)`,
  },
}));

const Minimal = (props) => {
  const { children } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Topbar className={classes.topbar} />
      <main className={classes.content}>{children}</main>
    </div>
  );
};

Minimal.propTypes = {
  children: PropTypes.node,
};

export default Minimal;
