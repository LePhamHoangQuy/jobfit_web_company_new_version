/* eslint-disable react/require-default-props */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Badge, Hidden, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import PostAddIcon from '@material-ui/icons/PostAdd';
import { connect } from 'react-redux';
import { socket } from '../../../../socket';
import Jobfit from '../../../../assets/images/jobfit.png';
import { DialogNoti } from '../../../../Components/DialogNoti';
import { getListNoti, makeSeenNoti } from '../../../../Actions/city.actions';

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: 'none',
  },
  flexGrow: {
    flexGrow: 1,
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
  },
  logo: {
    width: '45%',
  },
  post: {
    '& a': {
      textDecoration: 'none',
    },
    ' & span': {
      color: 'white',
    },
  },
}));

const Topbar = (props) => {
  const [notifications, setNoti] = useState([]);
  const [openNoti, setOpenNoti] = useState(false);
  const noti = localStorage.getItem('notifications')
    ? JSON.parse(localStorage.getItem('notifications'))
    : [];

  const {
    className,
    onSidebarOpen,
    getNotification,
    makeSeen,
    ...rest
  } = props;

  const classes = useStyles();

  const handleSignOut = () => {
    localStorage.clear();
    window.location.href = '/login';
  };

  useEffect(() => {
    socket.on('followNoti', (res) => {
      const message = { msg: res, type: 'follow' };
      const tempArr = [];
      tempArr.push(message);
      localStorage.setItem(
        'notifications',
        JSON.stringify(noti.concat(tempArr)),
      );
      setNoti(noti.concat(tempArr));
    });
  }, []);

  useEffect(() => {
    socket.on('applyCVNoti', (res) => {
      const message = {
        msg: `${res.employee_name} đã ứng tuyển vào vị trí ${res.post_name}`,
        type: 'apply',
      };
      const tempArr = [];
      tempArr.push(message);
      localStorage.setItem(
        'notifications',
        JSON.stringify(noti.concat(tempArr)),
      );
      setNoti(noti.concat(tempArr));
    });
  }, []);

  useEffect(() => {
    getNotification();
  }, []);

  return (
    <AppBar {...rest} className={clsx(classes.root, className)}>
      <Toolbar>
        <RouterLink to="/">
          <img className={classes.logo} alt="Logo" src={Jobfit} />
        </RouterLink>
        <div className={classes.flexGrow} />
        <RouterLink className={classes.post} to="/post-job">
          <IconButton color="inherit">
            <Badge>
              <PostAddIcon />
            </Badge>
          </IconButton>
        </RouterLink>
        <Hidden mdDown>
          <IconButton color="inherit" onClick={() => setOpenNoti(true)}>
            <Badge badgeContent={noti.length} color="error">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton
            onClick={handleSignOut}
            className={classes.signOutButton}
            color="inherit"
          >
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton color="inherit" onClick={onSidebarOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
      <DialogNoti
        open={openNoti}
        onClose={() => setOpenNoti(false)}
        notifications={noti}
        makeSeen={makeSeen}
      />
    </AppBar>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getNotification: () => {
      dispatch(getListNoti());
    },
    makeSeen: (ids) => {
      dispatch(makeSeenNoti(ids));
    },
  };
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func,
  getNotification: PropTypes.func,
  makeSeen: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(Topbar);
