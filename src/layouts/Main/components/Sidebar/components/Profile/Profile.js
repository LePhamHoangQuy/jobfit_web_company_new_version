/* eslint-disable react/require-default-props */
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content',
  },
  avatar: {
    width: 60,
    height: 60,
  },
  name: {
    marginTop: theme.spacing(1),
    textAlign: 'center',
  },
}));

const Profile = (props) => {
  const { user, className, ...rest } = props;
  const classes = useStyles();

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      {!_.isEmpty(user) && (
        <Avatar
          alt="Person"
          className={classes.avatar}
          component={RouterLink}
          src={user.user_info.avatar}
          to="/profile"
        />
      )}
      <Typography className={classes.name} variant="h4">
        {user.username}
      </Typography>
      <Typography variant="body2">{user.tax_code}</Typography>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.profile,
});

Profile.propTypes = {
  className: PropTypes.instanceOf(Object),
  user: PropTypes.instanceOf(Object),
};

export default connect(mapStateToProps, null)(Profile);
