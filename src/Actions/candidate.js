/* eslint-disable no-alert */
/* eslint-disable no-useless-catch */
/* eslint-disable no-use-before-define */
import { trackPromise } from 'react-promise-tracker';
import * as services from '../Services/candidate';
import { CANDIDATE } from '../Constants';

export const getCandidate = (companyId, offset) => {
  return async (dispatch) => {
    try {
      const candidates = await trackPromise(
        services.fetchCandidate(companyId, offset),
      );
      if (candidates.status === 200) {
        dispatch(success(candidates.data.data, candidates.data.total));
      }
    } catch (error) {
      dispatch(failed());
      throw error;
    }
  };
  function success(data, total) {
    return {
      type: CANDIDATE.FETCH_CANDIDATE_SUCCESS,
      payload: data,
      total,
    };
  }
  function failed() {
    return {
      type: CANDIDATE.FETCH_CANDIDATE_FAILED,
    };
  }
};

export const getProfile = (id) => {
  return async (dispatch) => {
    try {
      const res = await trackPromise(services.getProfileCandidate(id));
      if (res.status === 200) {
        dispatch(success(res.data.employee));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(data) {
    return {
      type: CANDIDATE.GET_PROFILE_CANDIDATE,
      payload: data,
    };
  }
};

export const getCandidateFollow = () => {
  return async (dispatch) => {
    try {
      const candidates = await trackPromise(services.getCandidateFollow());
      if (candidates.status === 200) {
        dispatch(success(candidates.data.followers));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(data, total) {
    return {
      type: CANDIDATE.FETCH_CANDIDATE_SUCCESS,
      payload: data,
      total,
    };
  }
};

export const rejectCV = (userId, postId) => {
  return async () => {
    try {
      const candidates = await trackPromise(services.rejectCV(userId, postId));
      if (candidates.status === 200) {
        alert('Từ chối ứng viên thành công');
      }
    } catch (error) {
      throw error;
    }
  };
};

export const approveCV = (userId, postId) => {
  return async () => {
    try {
      const candidates = await trackPromise(services.approveCV(userId, postId));
      if (candidates.status === 200) {
        alert('Chấp thuận ứng viên thành công');
      }
    } catch (error) {
      throw error;
    }
  };
};
