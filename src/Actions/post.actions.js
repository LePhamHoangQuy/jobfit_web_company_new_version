/* eslint-disable no-useless-catch */
/* eslint-disable no-use-before-define */
import { trackPromise } from 'react-promise-tracker';
import { postConstants, JOB_CATEGORY } from '../Constants/index';
import * as service from '../Services/post.service';

export const createPost = (body) => {
  return (dispatch) => {
    return trackPromise(
      service
        .createPost(body)
        .then((res) => {
          dispatch(createPostSuccess(res.data.data[0]));
          alert('Thêm thành công');
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function createPostSuccess(data) {
    return {
      type: postConstants.ADD_POST,
      payload: data,
    };
  }
};

export const updatePost = (body) => {
  return (dispatch) => {
    return trackPromise(
      service
        .updatePost(body)
        .then((res) => {
          // dispatch(createPostSuccess(res.data.data[0]));
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function updatePostSuccess(data) {
    return {
      type: postConstants.ADD_POST,
      payload: data,
    };
  }
};

export const setInputUpdate = (values) => {
  return {
    type: postConstants.SET_INPUT_UPDATE,
    payload: values,
  };
};

export const fetchPostByidCompany = (
  idCompany,
  postType,
  offset,
  jobType,
  jobCategory,
) => {
  return (dispatch) => {
    return trackPromise(
      service
        .fetchPostByidCompany(idCompany, postType, offset, jobType, jobCategory)
        .then((res) => {
          dispatch(fetchPostByidCompanySuccess(res.data.data, res.data.total));
        })
        .catch((err) => {
          dispatch(failed());
          throw err;
        }),
    );
  };
  function fetchPostByidCompanySuccess(data, total) {
    return {
      type: postConstants.FETCH_POST_BY_ID_COMPANY,
      payload: data,
      total,
    };
  }
  function failed() {
    return {
      type: postConstants.FETCH_POST_BY_ID_COMPANY_FAILED,
    };
  }
};

export const deletePostItem = (postId) => {
  return (dispatch) => {
    return trackPromise(
      service
        .deletePostItem(postId)
        .then(() => {
          dispatch(deletePostItemSuccess(postId));
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function deletePostItemSuccess(id) {
    return {
      type: postConstants.DELETE_POST,
      payload: { id },
    };
  }
};

export const fetchJobCategory = () => {
  return async (dispatch) => {
    try {
      const categories = await trackPromise(service.getJobCategory());
      if (categories.status === 200) {
        dispatch(fetchJobCategorySuccess(categories.data.data));
      }
    } catch (error) {
      throw error;
    }
  };
  function fetchJobCategorySuccess(data) {
    return {
      type: JOB_CATEGORY,
      payload: data,
    };
  }
};

export const fetchPostById = (id) => {
  return async (dispatch) => {
    try {
      const post = await trackPromise(service.getPostById(id));
      if (post.status === 200) {
        dispatch(success(post.data.data[0]));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(data) {
    return {
      type: postConstants.GET_POST_BY_ID,
      payload: data,
    };
  }
};

export const getCommentById = (id) => {
  return async (dispatch) => {
    try {
      const comments = await trackPromise(service.getCommentByPostId(id));
      console.log('getCommentById -> comments', comments);
      if (comments.status === 200) {
        dispatch(success(comments.data.data));
      }
    } catch (error) {
      dispatch(failed());
      throw error;
    }
  };
  function success(data) {
    return {
      type: postConstants.GET_COMMENT_BY_ID,
      payload: data,
    };
  }
  function failed() {
    return {
      type: 'GET_COMMENT_FAILD',
    };
  }
};

export const setComment = (data) => {
  return async (dispatch) => {
    try {
      const res = await trackPromise(service.setComment(data));
      if (res.status === 200) {
        dispatch(success(res.data.data));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(payload) {
    return {
      type: postConstants.SET_COMMENT,
      payload,
    };
  }
};

export const getJobPosition = () => {
  return async (dispatch) => {
    try {
      const positions = await trackPromise(service.getJobPositon());
      if (positions.status === 200) {
        dispatch(success(positions.data.data));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(data) {
    return {
      type: postConstants.GET_JOB_POSITION,
      payload: data,
    };
  }
};

export const getChildProSkill = () => {
  return async (dispatch) => {
    try {
      const skills = await trackPromise(service.getChildProSkill());
      if (skills.status === 200) {
        dispatch(success(skills.data.data));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(data) {
    return {
      type: postConstants.GET_CHILD_PRO_SKILL,
      payload: data,
    };
  }
};
