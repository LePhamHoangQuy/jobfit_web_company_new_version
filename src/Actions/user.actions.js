/* eslint-disable no-use-before-define */
import { trackPromise } from 'react-promise-tracker';
import { userConstants } from '../Constants/index';
import * as service from '../Services/user.service';

export const register = (user) => {
  return async (dispatch) => {
    try {
      const ret = await trackPromise(service.register(user));
      if (ret.status === 201) {
        dispatch(success());
      } else {
        dispatch(failure());
      }
    } catch (error) {
      dispatch(failure());
      throw error;
    }
  };
  function success() {
    return { type: userConstants.REGISTER_SUCCESS };
  }
  function failure() {
    return { type: userConstants.REGISTER_FAILURE };
  }
};

export const login = (email, password) => {
  return async (dispatch) => {
    try {
      const ret = await trackPromise(service.login(email, password));
      if (ret.status === 200) {
        const { accessToken, refreshToken, user } = ret.data;
        const { id, username } = user;
        localStorage.setItem('accessToken', accessToken);
        localStorage.setItem('refreshToken', refreshToken);
        localStorage.setItem('user_id', id);
        localStorage.setItem('username', username);
        dispatch(success(user));
        window.location.href = '/profile';
      }
    } catch (error) {
      dispatch(failure());
      throw error;
    }
  };
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure() {
    return { type: userConstants.LOGIN_FAILURE };
  }
};

export const updatePassword = (currentPassword, newPassword) => {
  return async () => {
    try {
      const res = await trackPromise(
        service.updatePassword(currentPassword, newPassword),
      );
      if (res.status === 200) {
        alert('Cập nhật mật khẩu thành công.');
      } else {
        alert('Cập nhật mật khẩu thất bại. Vui lòng thử lại');
      }
    } catch (error) {
      alert('Cập nhật mật khẩu thất bại. Vui lòng thử lại');
      throw error;
    }
  };
};

export const forgetPassword = (email, roleName) => {
  return async () => {
    try {
      const res = await trackPromise(service.forgetPassword(email, roleName));
      if (res.status === 200) {
        alert('Vui lòng kiểm tra email để nhận mật khẩu');
      }
    } catch (error) {
      alert('Có lỗi xảy ra. Vui lòng kiểm tra lại');
      throw error;
    }
  };
};
