/* eslint-disable no-underscore-dangle */
/* eslint-disable no-useless-catch */
/* eslint-disable no-use-before-define */
import { trackPromise } from 'react-promise-tracker';
import { cityConstants } from '../Constants';
import * as service from '../Services/city.service';

export const fetchProvince = () => {
  return (dispatch) => {
    return trackPromise(
      service
        .getAllProvince()
        .then((res) => {
          if (res.status === 200) {
            dispatch(fetchProvinceSuccess(res.data.data));
          }
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function fetchProvinceSuccess(data) {
    return {
      type: cityConstants.FETCH_PROVINCE,
      payload: data,
    };
  }
};

export const fetchDistrict = (provinceId) => {
  return (dispatch) => {
    return trackPromise(
      service
        .getDistrictByProvinceId(provinceId)
        .then((res) => {
          if (res.status === 200) {
            dispatch(fetchDistrictSuccess(res.data.data));
          }
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function fetchDistrictSuccess(data) {
    return {
      type: cityConstants.FETCH_DISTRICT,
      payload: data,
    };
  }
};

export const fetchWard = (districtId) => {
  return (dispatch) => {
    return trackPromise(
      service
        .getWardByDistrictId(districtId)
        .then((res) => {
          if (res.status === 200) {
            dispatch(fetchWardSuccess(res.data.data));
          }
        })
        .catch((err) => {
          throw err;
        }),
    );
  };
  function fetchWardSuccess(data) {
    return {
      type: cityConstants.FETCH_WARD,
      payload: data,
    };
  }
};

export const getListNoti = () => {
  return async () => {
    try {
      const notifications = await trackPromise(service.getNotiList());
      if (notifications.status === 200) {
        const noti = localStorage.getItem('notifications')
          ? JSON.parse(localStorage.getItem('notifications'))
          : [];
        const notiList = notifications.data.notis
          .filter((value) => value.type !== 'CV_APPROVED')
          .map((item) => {
            if (item.type === 'FOLLOW') {
              return { id: item._id, msg: item.content, type: 'follow' };
            }
            if (item.type === 'APPLIED') {
              return {
                msg: `${
                  JSON.parse(item.content).employee_name
                } đã ứng tuyển vào vị trí ${
                  JSON.parse(item.content).post_name
                }`,
                type: 'apply',
                id: item._id,
              };
            }
            return null;
          });
        localStorage.setItem(
          'notifications',
          JSON.stringify(noti.concat(notiList)),
        );
      }
    } catch (error) {
      throw error;
    }
  };
};

export const makeSeenNoti = (ids) => {
  return async () => {
    try {
      ids.forEach(async (id) => {
        await trackPromise(service.makeSeenNoti(id));
      });
    } catch (error) {
      throw error;
    }
  };
};
