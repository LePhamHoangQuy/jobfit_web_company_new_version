/* eslint-disable no-use-before-define */
/* eslint-disable no-useless-catch */
import { trackPromise } from 'react-promise-tracker';
import _ from 'lodash';
import { profileConstants } from '../Constants/index';
import * as service from '../Services/user.service';

export const getCompanyProfile = () => {
  return async (dispatch) => {
    try {
      const ret = await trackPromise(service.getCompanyProfile());
      if (ret.status === 200) {
        const data = _.omit(ret.data.profile, 'password');
        localStorage.setItem('profile', JSON.stringify(data));
        dispatch(getCompanyProfileSuccess(data));
      }
    } catch (error) {
      throw error;
    }
  };
  function getCompanyProfileSuccess(data) {
    return {
      type: profileConstants.FETCH_PROFILE,
      payload: data,
    };
  }
};

export const updateCompanyProfile = (data) => {
  return (dispatch) => {
    return trackPromise(
      service.updateCompanyProfile(data).then((res) => {
        if (res.status === 200) {
          dispatch(updateCompanyProfileSuccess(res.data.profile));
          alert('Cập nhật thành công. ');
        }
      }),
    );
  };
  function updateCompanyProfileSuccess(profile) {
    return {
      type: profileConstants.UPDATE_PROFILE,
      payload: profile,
    };
  }
};

export const uploadAvatar = (file) => {
  return async (dispatch) => {
    try {
      const res = await trackPromise(service.updateAvatar(file));
      if (res.status === 200) {
        dispatch(success(res.data.image.url));
      }
    } catch (error) {
      throw error;
    }
  };
  function success(url) {
    return {
      type: profileConstants.UPLOAD_AVATAR,
      payload: url,
    };
  }
};
