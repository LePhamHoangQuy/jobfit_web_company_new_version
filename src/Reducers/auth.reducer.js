/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
import { userConstants } from '../Constants';

const authReducer = (state = {}, action) => {
  switch (action.type) {
    case userConstants.LOGIN_SUCCESS:
      state = action.user;
      return { ...state, loggedIn: true };
    case userConstants.LOGIN_FAILURE:
      state = { loggedIn: false };
      return { ...state };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
};

export default authReducer;
