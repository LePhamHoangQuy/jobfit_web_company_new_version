/* eslint-disable no-param-reassign */
import { CANDIDATE } from '../Constants';

const candidate = (state = {}, action) => {
  switch (action.type) {
    case CANDIDATE.FETCH_CANDIDATE_SUCCESS: {
      state.data = action.payload;
      state.total = action.total;
      return { ...state };
    }
    case CANDIDATE.FETCH_CANDIDATE_FAILED:
      state.data = [];
      state.total = 0;
      return { ...state, isGet: false };
    default:
      return { ...state };
  }
};

export default candidate;
