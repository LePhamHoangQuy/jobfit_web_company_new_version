/* eslint-disable no-param-reassign */
import { postConstants } from '../Constants';

const jobPosition = (state = [], action) => {
  switch (action.type) {
    case postConstants.GET_JOB_POSITION:
      state = action.payload;
      return [...state];
    default:
      return [...state];
  }
};

export default jobPosition;
