/* eslint-disable no-param-reassign */
import { CANDIDATE } from '../Constants';

const profileCandidate = (state = {}, action) => {
  switch (action.type) {
    case CANDIDATE.GET_PROFILE_CANDIDATE: {
      state = action.payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default profileCandidate;
