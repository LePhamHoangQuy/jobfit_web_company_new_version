/* eslint-disable no-param-reassign */
import { profileConstants } from '../Constants';

const Profile = (state = {}, action) => {
  switch (action.type) {
    case profileConstants.FETCH_PROFILE:
      state = action.payload;
      return { ...state };
    case profileConstants.UPDATE_PROFILE:
      state = action.payload;
      return { ...state };
    default:
      return state;
  }
};

export default Profile;
