/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
import { profileConstants } from '../Constants';

const image = (state = { isUpload: false }, action) => {
  switch (action.type) {
    case profileConstants.UPLOAD_AVATAR:
      state.isUpload = true;
      state.src = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};
export default image;
