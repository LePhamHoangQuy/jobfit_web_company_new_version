/* eslint-disable no-param-reassign */
import { postConstants } from '../Constants';

const postReducer = (state = {}, action) => {
  switch (action.type) {
    case postConstants.ADD_POST: {
      if (!state.data && !state.total) {
        state.data = [];
        state.total = 0;
        state.data.push(action.payload);
        state.total += 1;
      } else {
        state.data.push(action.payload);
        state.total += 1;
      }
      return { ...state };
    }
    case postConstants.FETCH_POST_BY_ID_COMPANY: {
      state.data = action.payload;
      state.total = action.total;
      return { ...state };
    }
    case postConstants.FETCH_POST_BY_ID_COMPANY_FAILED:
      state.data = [];
      state.total = 0;
      return { ...state, isGet: false };
    case postConstants.DELETE_POST: {
      state.data = state.data.filter(
        (post) => post.postId !== action.payload.id,
      );
      state.total -= 1;
      return { ...state };
    }
    case postConstants.GET_POST_BY_ID:
      state = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default postReducer;
