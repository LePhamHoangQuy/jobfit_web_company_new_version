/* eslint-disable no-param-reassign */
import { JOB_CATEGORY } from '../Constants';

const category = (state = [], action) => {
  switch (action.type) {
    case JOB_CATEGORY:
      state = action.payload;
      return [...state];
    default:
      return state;
  }
};

export default category;
