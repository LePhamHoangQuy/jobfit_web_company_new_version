/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import { postConstants } from '../Constants';

const comment = (state = [], action) => {
  switch (action.type) {
    case postConstants.GET_COMMENT_BY_ID:
      state = action.payload;
      return [...state];
    case postConstants.SET_COMMENT:
      state = state.concat(action.payload);
      return [...state];
    case 'GET_COMMENT_FAILD':
      state = [];
      return [...state];
    default:
      return [...state];
  }
};

export default comment;
