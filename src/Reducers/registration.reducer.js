/* eslint-disable no-param-reassign */
import { userConstants } from '../Constants';

const registrationReducer = (state = {}, action) => {
  switch (action.type) {
    case userConstants.REGISTER_SUCCESS:
      state = { registering: true };
      return { ...state };
    case userConstants.REGISTER_FAILURE:
      state = { registering: false };
      return { ...state };
    default:
      return {};
  }
};

export default registrationReducer;
