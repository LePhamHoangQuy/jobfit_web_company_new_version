/* eslint-disable no-param-reassign */
import { postConstants } from '../Constants';

const inputUpdatePost = (state = {}, action) => {
  switch (action.type) {
    case postConstants.SET_INPUT_UPDATE: {
      state = action.payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default inputUpdatePost;
