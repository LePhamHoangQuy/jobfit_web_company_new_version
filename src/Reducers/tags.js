/* eslint-disable no-param-reassign */
import { postConstants } from '../Constants';

const tags = (state = [], action) => {
  switch (action.type) {
    case postConstants.GET_CHILD_PRO_SKILL:
      state = action.payload;
      return [...state];
    default:
      return [...state];
  }
};

export default tags;
