import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import registration from './registration.reducer';
import auth from './auth.reducer';
import alert from './alert.reducer';
import posts from './post.reducer';
import places from './place.reducer';
import profile from './profile.reducer';
import jobCategory from './jobCategory';
import candidates from './candidate';
import profileCandidate from './profileCandidate';
import image from './image';
import inputUpdatePost from './inputUpdatePost';
import comment from './comment';
import jobPosition from './jobPosition';
import tags from './tags';

const appReducers = combineReducers({
  form: formReducer,
  registration,
  alert,
  auth,
  posts,
  places,
  profile,
  jobCategory,
  candidates,
  profileCandidate,
  image,
  inputUpdatePost,
  comment,
  jobPosition,
  tags,
});

export default appReducers;
