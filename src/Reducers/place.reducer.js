import { cityConstants } from '../Constants';

const initialState = {
  provinces: [],
  districts: [],
  wards: [],
};

const placeReducers = (state = initialState, action) => {
  switch (action.type) {
    case cityConstants.FETCH_PROVINCE: {
      return { ...state, provinces: action.payload };
    }
    case cityConstants.FETCH_DISTRICT: {
      return { ...state, districts: action.payload };
    }
    case cityConstants.FETCH_WARD: {
      return { ...state, wards: action.payload };
    }
    default:
      return state;
  }
};

export default placeReducers;
