/* eslint-disable react/require-default-props */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import { renderSelectField } from '../../../Components/CustomField/SelectField';

const styles = {
  form: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  fieldA: {
    display: 'flex',
    width: '20%',
    justifyContent: 'space-around',
  },
  title: {
    marginTop: 22,
  },
  fieldB: {
    display: 'flex',
    width: '28%',
    justifyContent: 'space-around',
  },
  btn: {
    marginTop: 14,
  },
};

let FilterForm = ({ classes, handleSubmit, jobCategory, handleReset }) => {
  return (
    <Paper>
      <form onSubmit={handleSubmit} className={classes.form}>
        <div className={classes.fieldA}>
          <p className={classes.title}>Hình thức</p>
          <Field name="jobType" component={renderSelectField}>
            <option value="" disabled />
            <option value="FULLTIME">Full - time</option>
            <option value="PARTTIME">Part - time</option>
          </Field>
        </div>
        <div className={classes.fieldB}>
          <p className={classes.title}>Ngành nghề</p>
          <Field name="jobCategory" component={renderSelectField}>
            <option value="" disabled />
            {Array.isArray(jobCategory) &&
              jobCategory.map(({ id, categoryName }) => (
                <option key={id} value={categoryName}>
                  {categoryName}
                </option>
              ))}
          </Field>
        </div>
        <div className={classes.btn}>
          <Button type="submit" variant="contained" color="primary">
            Tìm kiếm
          </Button>
          <Button color="primary" onClick={handleReset}>
            <RotateLeftIcon />
          </Button>
        </div>
      </form>
    </Paper>
  );
};

FilterForm = reduxForm({
  form: 'filterPost',
})(FilterForm);

FilterForm.propTypes = {
  classes: PropTypes.instanceOf(Object),
  handleSubmit: PropTypes.func,
  jobCategory: PropTypes.instanceOf(Object),
  handleReset: PropTypes.func,
};

export default withStyles(styles)(FilterForm);
