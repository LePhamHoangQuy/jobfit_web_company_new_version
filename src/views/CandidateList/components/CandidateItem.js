/* eslint-disable react/require-default-props */
/* eslint-disable default-case */
import React from 'react';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Dropdown from 'react-bootstrap/Dropdown';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import Chip from '@material-ui/core/Chip';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const styles = {
  btn: {
    marginRight: 44,
  },
};

const CandidateItem = ({
  candidate,
  index,
  getProfileCandidate,
  getPostDetail,
}) => {
  const handleShowDetail = () => {
    getProfileCandidate(candidate.userId);
    getPostDetail(candidate.postId);
  };

  const getStatus = (status) => {
    switch (status) {
      case 'APPLIED':
        return <Chip label="Đang xem xét" />;
      case 'CV_APPROVED':
        return (
          <Chip
            style={{ backgroundColor: '#1976d2', color: 'white' }}
            label="Đã đồng ý"
          />
        );
      case 'CV_REJECTED':
        return (
          <Chip
            style={{ backgroundColor: 'rgb(220, 0, 78)', color: 'white' }}
            label="Đã từ chối"
          />
        );
      default:
        return null;
    }
  };

  const createdTime = new Date(_.get(candidate, 'createdTime', ''));
  return (
    <>
      <tr>
        <td>{index}</td>
        <td>{createdTime.toLocaleDateString()}</td>
        <td>{candidate.fullName}</td>
        <td>{candidate.postName}</td>
        <td>{getStatus(candidate.applyStatus)}</td>
        <td>
          <Dropdown>
            <Dropdown.Toggle variant="danger" id="dropdown-basic">
              <FormatListBulletedIcon />
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>
                <Link onClick={handleShowDetail} to="/candidate-detail">
                  Xem chi tiết
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>Phê duyệt</Dropdown.Item>
              <Dropdown.Item>Từ chối</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </td>
      </tr>
    </>
  );
};

CandidateItem.propTypes = {
  candidate: PropTypes.instanceOf(Object),
  index: PropTypes.number,
  getProfileCandidate: PropTypes.func,
  getPostDetail: PropTypes.func,
};

export default withStyles(styles)(CandidateItem);
