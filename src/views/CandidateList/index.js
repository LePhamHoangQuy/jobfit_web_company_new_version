/* eslint-disable react/no-array-index-key */
/* eslint-disable react/require-default-props */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Pagination from '@material-ui/lab/Pagination';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import { getCandidate, getProfile } from '../../Actions/candidate';
import { fetchPostById } from '../../Actions/post.actions';
import CandidateItem from './components/CandidateItem';
import CandidateList from './components';

const styles = {
  pagination: {
    marginTop: 6,
    display: 'flex',
    justifyContent: 'center',
  },
};

const Candidate = ({
  getCandidateByIdCompany,
  candidates,
  classes,
  getProfileCandidate,
  getPostDetail,
}) => {
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const id = localStorage.getItem('user_id');
    getCandidateByIdCompany(id, offset);
  }, [offset]);

  const showCandidateItem = (data) => {
    return Array.isArray(data) && data.length > 0 ? (
      data.map((candidate, index) => (
        <CandidateItem
          getProfileCandidate={getProfileCandidate}
          getPostDetail={getPostDetail}
          key={index}
          candidate={candidate}
          index={index + 1}
        />
      ))
    ) : (
      <tr style={{ textAlign: 'center' }}>
        <td colSpan="12">Chưa có ứng viên nào.</td>
      </tr>
    );
  };

  const handleChangePage = (event, value) => {
    setOffset((value - 1) * 10);
  };

  return (
    <>
      <CandidateList>{showCandidateItem(candidates.data)}</CandidateList>
      <div className={classes.pagination}>
        <Pagination
          count={Math.ceil(candidates.total / 10)}
          onChange={handleChangePage}
        />
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  candidates: state.candidates,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidateByIdCompany: (id, offset) => {
      dispatch(getCandidate(id, offset));
    },
    getProfileCandidate: (id) => {
      dispatch(getProfile(id));
    },
    getPostDetail: (id) => {
      dispatch(fetchPostById(id));
    },
  };
};

Candidate.propTypes = {
  getCandidateByIdCompany: PropTypes.func,
  getPostDetail: PropTypes.func,
  getProfileCandidate: PropTypes.func,
  candidates: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(Candidate);
