/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/require-default-props */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import {
  fetchProvince,
  fetchDistrict,
  fetchWard,
} from '../../Actions/city.actions';
import {
  updateCompanyProfile,
  uploadAvatar,
  getCompanyProfile,
} from '../../Actions/profile.actions';
import AccountProfile from './components/account';
import AccountDetails from './components/detail';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}));

const Profile = (props) => {
  const classes = useStyles();
  const { getProfile, avatar, profile, updateProfile, updateAvatar } = props;
  const initialValues = {
    username: !_.isEmpty(profile) && profile.username,
    email: !_.isEmpty(profile) && profile.email,
    phone: !_.isEmpty(profile) && profile.user_info.phone,
    description: !_.isEmpty(profile) && profile.description,
    address: !_.isEmpty(profile) && profile.user_info.address,
    tax_code: !_.isEmpty(profile) && profile.tax_code,
    avatar: avatar.isUpload && avatar.src,
  };

  useEffect(() => {
    getProfile();
  }, []);

  const handleUpdate = (values) => {
    updateProfile(values);
  };

  return (
    <div>
      <div className={classes.root}>
        <Grid container spacing={4}>
          <Grid item lg={4} md={6} xl={4} xs={12}>
            <AccountProfile
              avatar={avatar}
              updateAvatar={updateAvatar}
              profile={profile}
            />
          </Grid>
          <Grid item lg={8} md={6} xl={8} xs={12}>
            <AccountDetails
              onSubmit={handleUpdate}
              initialValues={initialValues}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  avatar: state.image,
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getProvince: () => {
      dispatch(fetchProvince());
    },
    getDistrict: (provinceId) => {
      dispatch(fetchDistrict(provinceId));
    },
    getWard: (districtId) => {
      dispatch(fetchWard(districtId));
    },
    updateProfile: (data) => {
      dispatch(updateCompanyProfile(data));
    },
    updateAvatar: (file) => {
      dispatch(uploadAvatar(file));
    },
    getProfile: () => {
      dispatch(getCompanyProfile());
    },
  };
};

Profile.propTypes = {
  updateProfile: PropTypes.func,
  avatar: PropTypes.instanceOf(Object),
  getProfile: PropTypes.func,
  updateAvatar: PropTypes.func,
  profile: PropTypes.instanceOf(Object),
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
