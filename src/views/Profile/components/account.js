/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Avatar,
  Typography,
  Divider,
  Button,
} from '@material-ui/core';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {},
  details: {
    display: 'flex',
  },
  avatar: {
    marginLeft: 'auto',
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0,
    '& img': {
      height: 'auto',
      width: '100%',
    },
  },
  progress: {
    marginTop: theme.spacing(2),
  },
  uploadButton: {
    marginRight: theme.spacing(2),
    textAlign: 'center',
    '& input': {
      display: 'none',
    },
  },
}));

const AccountProfile = (props) => {
  const { profile, updateAvatar, avatar, className, ...rest } = props;

  const classes = useStyles();

  const createdTime = new Date(_.get(profile.user_info, 'created_at', ''));

  const handleOnChange = (e) => {
    const image = e.target.files[0];
    const formData = new FormData();
    formData.append('image', image, image.name);
    updateAvatar(formData);
  };

  const avatarCom = _.get(profile, 'user_info', '');

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <div className={classes.details}>
          <div>
            <Typography gutterBottom variant="h4">
              {_.get(profile, 'username', '')}
            </Typography>
            <Typography
              className={classes.locationText}
              color="textSecondary"
              variant="body1"
            >
              {_.get(profile, 'email', '')}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {createdTime.toLocaleDateString()}
            </Typography>
          </div>
          {avatar.isUpload === false ? (
            <Avatar className={classes.avatar} src={avatarCom.avatar} />
          ) : (
            <Avatar className={classes.avatar} src={avatar.src} />
          )}
        </div>
      </CardContent>
      <Divider />
      <div className={classes.uploadButton}>
        <input
          onChange={handleOnChange}
          accept=".jpg, .png, .jpeg"
          className={classes.input}
          id="contained-button-file"
          multiple
          type="file"
        />
        <label htmlFor="contained-button-file">
          <Button variant="contained" color="primary" component="span">
            Cập nhật ảnh đại diện
          </Button>
        </label>
      </div>
    </Card>
  );
};

AccountProfile.propTypes = {
  className: PropTypes.string,
  profile: PropTypes.instanceOf(Object),
  updateAvatar: PropTypes.func,
  avatar: PropTypes.instanceOf(Object),
};

export default AccountProfile;
