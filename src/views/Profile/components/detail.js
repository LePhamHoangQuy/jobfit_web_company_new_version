/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable import/no-mutable-exports */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
} from '@material-ui/core';
import { Field, reduxForm } from 'redux-form';
import EditorField from '../../../Components/CustomField/Editor';
import TextField from '../../../Components/CustomField/TextField';

const useStyles = makeStyles(() => ({
  root: {},
}));

let AccountDetails = (props) => {
  const { handleSubmit, className } = props;
  const classes = useStyles();

  return (
    <Card className={clsx(classes.root, className)}>
      <form onSubmit={handleSubmit}>
        <CardHeader
          subheader="Cập nhật thông tin công ty tại đây"
          title="Thông tin công ty"
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <div>
                <Field
                  component={TextField}
                  fullWidth
                  label="Tên công ty"
                  margin="dense"
                  name="username"
                  required
                  variant="outlined"
                />
              </div>
            </Grid>
            <Grid item md={6} xs={12}>
              <div>
                <Field
                  name="tax_code"
                  component={TextField}
                  fullWidth
                  label="Mã số thuế"
                  margin="dense"
                  required
                  variant="outlined"
                />
              </div>
            </Grid>
            <Grid item md={6} xs={12}>
              <div>
                <Field
                  name="phone"
                  component={TextField}
                  fullWidth
                  label="Số điện thoại"
                  margin="dense"
                  required
                  variant="outlined"
                />
              </div>
            </Grid>
            <Grid item md={6} xs={12}>
              <div>
                <Field
                  name="address"
                  component={TextField}
                  fullWidth
                  label="Địa chỉ"
                  margin="dense"
                  required
                  rowsMax={6}
                  multiline
                  variant="outlined"
                />
              </div>
            </Grid>
            <Grid item md={12} xs={12}>
              <div>
                <label>Giới thiệu công ty</label>
                <EditorField
                  key="field"
                  name="description"
                  id="inputEditorText"
                  disabled={false}
                  placeholder="Type here"
                />
              </div>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button type="submit" color="primary" variant="contained">
            Cập nhật
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails = reduxForm({
  form: 'profileForm',
  enableReinitialize: true,
})(AccountDetails);

AccountDetails.propTypes = {
  className: PropTypes.string,
};

export default AccountDetails;
