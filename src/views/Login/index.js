/* eslint-disable react/require-default-props */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { SubmissionError, reset } from 'redux-form';
import LoginForm from '../../Components/LoginForm';
import { login, forgetPassword } from '../../Actions/user.actions';
import NavBar from '../../Components/NavBar/navbarsimple';
import ForgotForm from '../../Components/ForgotPassword';

const Login = ({ loginCompany, user, forgot, resetFormForgot }) => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const submit = (values) => {
    const { email, password } = values;
    loginCompany(email, password);
  };

  const handleForgot = (values) => {
    if (
      values.email &&
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    )
      throw new SubmissionError({ email: 'email không hợp lệ' });
    forgot(values.email, 'COMPANY');
    handleClose();
    resetFormForgot();
  };

  return (
    <div>
      <NavBar />
      <LoginForm
        user={user}
        onSubmit={submit}
        handleClickOpen={handleClickOpen}
      />
      <ForgotForm
        open={open}
        handleClose={handleClose}
        onSubmit={handleForgot}
      />
    </div>
  );
};

const mapStateToProp = (state) => ({
  user: state.auth,
});

const mapDispatchToProp = (dispatch, ownProps) => {
  return {
    loginCompany: (email, password) => {
      dispatch(login(email, password, ownProps));
    },
    forgot: (email, roleName) => {
      dispatch(forgetPassword(email, roleName));
    },
    resetFormForgot: () => {
      dispatch(reset('forgotForm'));
    },
  };
};

Login.propTypes = {
  user: PropTypes.instanceOf(Object),
  loginCompany: PropTypes.func,
  forgot: PropTypes.func,
  resetFormForgot: PropTypes.func,
};

export default withRouter(connect(mapStateToProp, mapDispatchToProp)(Login));
