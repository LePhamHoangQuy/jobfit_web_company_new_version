import { SubmissionError } from 'redux-form';

export const validation = (values) => {
  if (values.username && values.username.length > 50)
    throw new SubmissionError({ username: 'Tên tài khoản không quá' });
  if (values.username && values.username.length < 3)
    throw new SubmissionError({
      username: 'Tên tài khoản phải trên 8 ký tự',
    });
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  )
    throw new SubmissionError({ email: 'email không hợp lệ' });
  if (values.password && values.password.length < 8)
    throw new SubmissionError({ password: 'Mật khẩu phải trên 8 ký tự' });
  if (values.password && values.passwordConfirm) {
    if (values.password !== values.passwordConfirm)
      throw new SubmissionError({
        passwordConfirm: 'Mật khẩu nhập lại không trùng khớp',
      });
  }
};
