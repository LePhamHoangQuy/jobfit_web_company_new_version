/* eslint-disable react/require-default-props */
/* eslint-disable import/no-unresolved */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { reset } from 'redux-form';
import _ from 'lodash';
import { register } from '../../Actions/user.actions';
import NavBar from '../../Components/NavBar/navbarsimple';
import RegisterForm from '../../Components/Register/RegisterForm';
import { validation } from './validate';

const styles = {
  page: {
    height: '100vh',
    width: '100vw',
  },
};
class RegisterPage extends Component {
  submit = (values) => {
    validation(values);
    const user = _.omit(values, 'passwordConfirm');
    this.props.register({ ...user, roleName: 'COMPANY' });
  };

  render() {
    const { classes, registration } = this.props;
    return (
      <div className={classes.page}>
        <NavBar />
        <RegisterForm registration={registration} onSubmit={this.submit} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    registration: state.registration,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    register: (user) => {
      dispatch(register(user));
    },
    resetForm: () => {
      dispatch(reset('registerForm'));
    },
  };
};

RegisterPage.propTypes = {
  classes: PropTypes.instanceOf(Object),
  register: PropTypes.func,
  registration: PropTypes.instanceOf(Object),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(RegisterPage);
