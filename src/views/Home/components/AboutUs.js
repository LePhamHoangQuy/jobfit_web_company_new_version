/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import companyLogo from '../../../assets/images/company.png';
import seasonJob from '../../../assets/images/seasonal-jobs-icon.png';

const AboutUs = () => (
  <div className="container-custom" style={{ marginTop: '-17px' }}>
    <div className="title">
      <p>Về chúng tôi</p>
    </div>
    <div className="main-info">
      <p>
        Đối với nhà tuyển dụng, thay vì phải tiếp nhận nhiều hồ sơ không được
        chọn lọc
        <br />
        hệ thống sẽ tự động đề xuất các ứng viên phù hợp nhất với vị trí mà nhà
        tuyển dụng cần tìm
      </p>
    </div>
    <div className="about-logo">
      <img className="company-logo" src={companyLogo} />
      <img className="season-job-logo" src={seasonJob} />
    </div>
    <div className="other-info">
      <p>
        Bạn có thể là một nhà tuyển dụng chuyên nghiệp
        <br /> cho những công ty lớn
      </p>
      <p>
        Bạn cũng có thể là một cá nhân có nhu cầu <br />
        tìm kiếm nhân lực thời vụ
      </p>
    </div>
  </div>
);

export default AboutUs;
