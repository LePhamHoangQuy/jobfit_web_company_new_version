import React from 'react';
import PartnerLogo from '../../../assets/images/partner.png';

const OurPartner = () => (
  <div className="container-custom">
    <div className="title">
      <p>Nhà tuyển dụng tiêu biểu</p>
    </div>
    <div className="partner-logo">
      <img src={PartnerLogo} alt="logo" />
    </div>
  </div>
);

export default OurPartner;
