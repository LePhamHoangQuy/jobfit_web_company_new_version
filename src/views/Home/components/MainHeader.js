import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import connectLogo from '../../../assets/images/connect.jpg';
import background from '../../../assets/images/background.jpg';

const styles = {
  row: {
    display: 'flex',
    width: '100%',
    background: '#54A0FF',
    height: '100vh',
    backgroundImage: `linear-gradient(45deg,rgba(56, 182, 199, 0.8) 55%,rgba(84,160,255)), url(${background})`,
  },
  headerImg: {
    textAlign: 'end',
    width: '40%',
    marginTop: 168,
    '& img': {
      width: '65%',
      marginRight: 50,
    },
  },
  headerText: {
    display: 'inline-block',
    textAlign: 'center',
    margin: '130px 0 0 40px',
    color: 'white',
    '& h2': {
      fontSize: 40,
    },
    '& p': {
      fontSize: 24,
      padding: '0 40px',
      marginBottom: 40,
    },
    '& a': {
      background: '#FF9F43',
      fontSize: 24,
      padding: '10px 30px',
      fontWeight: 'bold',
      '&:hover': {
        cursor: 'pointer',
        background: '#E58F3C',
      },
    },
  },
};

const MainHeader = (props) => {
  const { classes } = props;
  return (
    <div className={classes.row}>
      <div className={classes.headerText}>
        <h2>
          Đăng tin tuyển dụng và tìm ứng viên
          <br /> nhanh nhất với <span>JobFit</span>
        </h2>
        <p>
          <strong>JobFit </strong>là một hệ thống thông minh bằng cách đề xuất{' '}
          <br />
          những ứng viên phù hợp nhất đến bạn
        </p>
        <a>Đăng tin ngay</a>
      </div>
      <div className={classes.headerImg}>
        <img src={connectLogo} alt="logo" />
      </div>
    </div>
  );
};

MainHeader.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(MainHeader);
