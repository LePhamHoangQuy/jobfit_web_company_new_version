/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Logout } from '../../../Components/Logout';
import Jobfit from '../../../assets/images/jobfit.png';

const styles = {
  navBar: {
    background: `linear-gradient(90deg, rgba(9, 31, 38, 1) 0%, rgba(60, 130, 147, 1) 43%, rgba(76, 103, 159, 1) 67%, rgba(57, 96, 125, 1) 100%, rgba(43, 136, 136, 1) 100%, rgba(84, 64, 24, 1) 100%, rgba(0, 212, 255, 1) 100%, rgba(42, 49, 224, 1) 100%)`,
    minHeight: '5rem',
    color: 'white',
  },
  logo: {
    display: 'inline-block',
    margin: '-14px 0 0 20px',
  },
  logoImg: {
    width: '50%',
  },
  register: {
    background: '#00d600',
    padding: '8px 25px',
  },
  mainNavBar: {
    display: 'inline-block',
    float: 'right',
    margin: 22,
    '& a': {
      marginRight: 36,
      color: 'white',
      textDecoration: 'none',
    },
    '& :hover': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  },
};

const NavBar = (props) => {
  const { classes } = props;
  const username = localStorage.getItem('username');
  return (
    <nav className={classes.navBar}>
      <div className={classes.logo}>
        <Link to="/">
          <img className={classes.logoImg} alt="Logo" src={Jobfit} />
        </Link>
      </div>
      <div className={classes.mainNavBar}>
        <a href="#about">Giới thiệu</a>
        <a href="#partner">Nhà tuyển dụng</a>
        <a href="#footer">Tải App</a>
        {username ? (
          <>
            <Link to="/profile">{`Xin chào, ${username}`}</Link>
            <Link onClick={() => Logout()} to="#">
              Đăng xuất
            </Link>
          </>
        ) : (
          <>
            <Link to="/login">Đăng nhập</Link>
            <Link to="/register" className={classes.register}>
              Đăng ký
            </Link>
          </>
        )}
      </div>
    </nav>
  );
};

NavBar.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(NavBar);
