import React from 'react'
import Logo from '../../../assets/images/Logo.PNG'
import AppStore from '../../../assets/images/AppStore.png'
import CHPlay from '../../../assets/images/CHPlay.png'
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';

let Footer = () => {
    return (
        <div className="container-custom" > 
            <div className="header-footer">
                <div className="footer-logo">
                    <img src={Logo} alt='logo' />
                </div>
                <div className="footer-link">
                    <ul>
                        <li>
                            <a href="#">Giới thiệu</a>
                        </li>
                        <li>
                            <a href="#">Điều khoản</a>
                        </li>
                        <li>
                            <a href="#">Bảo mật</a>
                        </li>
                        <li>
                            <a href="#">Dành cho nhà tuyển dụng</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="#">FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="body-footer">
                <div className="header-body-footer">
                    <div className="condition">
                        <h1>Ứng viên theo địa điểm</h1>
                        <ul>
                            <li>
                                <a href="#">TP HCM</a>
                            </li>
                            <li>
                                <a href="#">Hà Nội</a>
                            </li>
                            <li>
                                <a href="#">Cần Thơ</a>
                            </li>
                            <li>
                                <a href="#">Đà Nẵng</a>
                            </li>
                            <li>
                                <a href="#">Xem tất cả >>
                           </a>
              </li>
            </ul>
          </div>
          <div className="condition">
            <h1>Ứng viên theo loại hình</h1>
            <ul>
              <li>
                <a href="#">Intern</a>
              </li>
              <li>
                <a href="#">Fresher</a>
              </li>
              <li>
                <a href="#">Manager</a>
              </li>
              <li>
                <a href="#">Bán thời gian</a>
              </li>
              <li>
                <a href="#">Xem tất cả >>
                           </a>
              </li>
            </ul>
          </div>
          <div className="condition">
            <h1>Ứng viên theo ngành nghề</h1>
            <ul>
              <li>
                <a href="#">Công nghệ thông tin</a>
              </li>
              <li>
                <a href="#">Ngân hàng</a>
              </li>
              <li>
                <a href="#">Điện tử viễn thông</a>
              </li>
              <li>
                <a href="#">Hóa học</a>
              </li>
              <li>
                <a href="#">Xem tất cả >>
                           </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="footer-body-footer">
          <div className="description">
            <ul>
              <li className="list-title">
                Công Ty Cổ Phần JobFit
                            </li>
              <li>
                <LocationOnIcon /> 227 Nguyễn Văn Cừ, Phường 7, Quận 5, TP Hồ Chí Minh
                            </li>
              <li>
                <EmailIcon /> cskh@jobfit.vn
                            </li>
              <li>
                <PhoneIcon /> 0123777473
                            </li>
            </ul>
          </div>
          <div className="link-contact">
            <ul>
              <li className="list-title">
                Có thể liên hệ với chúng tôi qua
                            </li>
              <li>
                <a href="#">
                  <FacebookIcon />
                                    https://www.facebook.com/jobfit
                                </a>
              </li>
              <li>
                <a href="#">
                  <InstagramIcon />
                                    https://www.instagram.com/jobfit
                                </a>
              </li>
              <li>
                <a href="#">
                  <TwitterIcon />
                                    https://www.twitter.com/jobfit
                                </a>
              </li>
            </ul>
          </div>
          <div className="link-down-app">
            <h1>Ứng dụng di động</h1>
            <a href="#">
              <img src={AppStore}></img>
            </a>
            <a href="#">
              <img src={CHPlay}></img>
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer