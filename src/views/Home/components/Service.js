/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Done } from '@material-ui/icons';

const Service = () => (
  <div className="container-custom" style={{ marginTop: '-20px' }}>
    <div className="title">
      <p style={{ padding: '0px' }}>Dịch Vụ</p>
    </div>
    <div className="service-info">
      <div className="service-card">
        <h3>Gói Platinum</h3>
        <p className="service-price">
          2.000.000 VND<span>/ tháng</span>
        </p>
        <div className="service-details">
          <Done />
          <p>Đăng tin không giới hạn trong vòng 1 tháng</p>
        </div>
        <div className="btn">
          <a>Đăng ký</a>
        </div>
      </div>
      <div className="service-card">
        <h3>Gói Gold</h3>
        <p className="service-price">
          800.000 VND<span>/ tháng</span>
        </p>
        <div className="service-details">
          <Done />
          <p>Đăng tin không quá 30 tin trong vòng 1 tháng</p>
        </div>
        <div className="btn">
          <a>Đăng ký</a>
        </div>
      </div>
    </div>
  </div>
);

export default Service;
