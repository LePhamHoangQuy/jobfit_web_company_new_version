import React from 'react';
import NavBar from './components/NavBar';
import MainHeader from './components/MainHeader';
import OurPartner from './components/OurPartner';
import AboutUs from './components/AboutUs';
import './styles.scss';
import Service from './components/Service';
import Footer from './components/Footer';

const HomePage = () => (
  <div>
    <header>
      <NavBar />
      <MainHeader />
    </header>
    <section id="partner">
      <OurPartner />
    </section>
    <section id="about">
      <AboutUs />
    </section>
    <section id="service">
      <Service />
    </section>
    <section id="footer">
      <Footer />
    </section>
  </div>
);

export default HomePage;
