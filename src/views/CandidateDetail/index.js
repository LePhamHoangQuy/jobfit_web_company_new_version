/* eslint-disable react/require-default-props */
/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import PhoneIcon from '@material-ui/icons/Phone';
import PersonIcon from '@material-ui/icons/Person';
import HomeIcon from '@material-ui/icons/Home';
import SchoolIcon from '@material-ui/icons/School';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import SubjectIcon from '@material-ui/icons/Subject';
import ProgressBar from 'react-bootstrap/ProgressBar';
import DateRangeIcon from '@material-ui/icons/DateRange';
import WcIcon from '@material-ui/icons/Wc';
import 'react-select2-wrapper/css/select2.css';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import Button from '@material-ui/core/Button';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { formatter } from '../../Helpers/ToCurrency';
import { rejectCV, approveCV } from '../../Actions/candidate';

const styles = {
  back: {
    position: 'relative',
    top: 50,
    zIndex: 100,
    '& a': {
      textDecoration: 'none',
    },
  },
};

const CandidateDetail = ({
  post,
  candidate,
  classes,
  history,
  reject,
  approve,
}) => {
  const infoCandidate = _.get(candidate, 'user_info', '');

  const handleReject = () => {
    reject(infoCandidate.empl_user_id.toString(), post.postId);
  };

  const handleApprove = () => {
    approve(infoCandidate.empl_user_id.toString(), post.postId);
  };
  return (
    <Container style={{ marginTop: '15px' }}>
      <div className={classes.back}>
        <Button onClick={() => history.goBack()} color="primary">
          <KeyboardBackspaceIcon />
        </Button>
      </div>
      <div className="candidate-detail">
        {_.isEmpty(candidate) && _.isEmpty(post) ? null : (
          <Row>
            <Col lg={{ span: 12, offset: 0 }}>
              <div className="container-candidate-header">
                <Row>
                  <Col lg="12">
                    <div
                      className="title text-center"
                      style={{ padding: '15px' }}
                    >
                      <h3 style={{ color: 'gray' }}>THÔNG TIN ỨNG VIÊN</h3>
                      <h6 style={{ color: 'gray' }}>(CV ứng viên)</h6>
                    </div>
                  </Col>
                  <Col lg={{ span: 9, offset: 2 }}>
                    <Row>
                      <Col lg="4" className="text-right">
                        <div className="ava-candidate">
                          <img src={infoCandidate.avatar} />
                        </div>
                      </Col>
                      <Col lg="8">
                        <div className="candidate-info">
                          <h1>{candidate.username}</h1>
                          <h4>{post.name}</h4>
                        </div>
                        <div className="sub-info">
                          <Row>
                            <Col lg="12">
                              <h6>{candidate.email}</h6>
                            </Col>
                            <Col lg="12">
                              <h6>{infoCandidate.phone}</h6>
                            </Col>
                            <Col lg="12">
                              <h6>{candidate.degree}</h6>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
              <div className="container-candidate-body">
                <Row>
                  <Col lg="12">
                    <div className="private-info">
                      <Row style={{ margin: '0px' }}>
                        <Col
                          lg="12"
                          className="container-content personal-info"
                        >
                          <h4 className="main-title">Thông tin cá nhân</h4>
                          <Row style={{ margin: '0px' }}>
                            <Col lg="6" className="info">
                              <div>
                                <PersonIcon />
                                <span>{candidate.username}</span>
                              </div>
                            </Col>
                            <Col lg="6" className="info">
                              <div>
                                <DateRangeIcon />
                                <span>{candidate.dob}</span>
                              </div>
                            </Col>

                            <Col lg="6" className="info">
                              <div>
                                <WcIcon />
                                <span>
                                  {candidate.gender === 1 ? `Nam` : `Nữ`}
                                </span>
                              </div>
                            </Col>

                            <Col lg="6" className="info">
                              <div>
                                <HomeIcon />
                                <span>{infoCandidate.address}</span>
                              </div>
                            </Col>
                            <Col lg="6" className="info">
                              <div>
                                <SchoolIcon />
                                <span>Đại học Khoa Học Tự Nhiên</span>
                              </div>
                            </Col>
                            <Col lg="6" className="info">
                              <div>
                                <ShowChartIcon />
                                <span>Đại học</span>
                              </div>
                            </Col>
                            <Col lg="6" className="info">
                              <div>
                                <PhoneIcon />
                                <span>{infoCandidate.phone}</span>
                              </div>
                            </Col>
                            <Col lg="6" className="info">
                              <div>
                                <SubjectIcon />
                                <span>Công nghệ thông tin</span>
                              </div>
                            </Col>
                          </Row>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Kỹ năng chuyên môn</h4>
                          {candidate.hard_skills &&
                            candidate.hard_skills.map((skill, index) => (
                              <Col key={index} lg="12" className="info-skill">
                                <div>
                                  <img src={skill.avatar} />
                                  <span>{skill.name}</span>
                                  <span>{`${skill.exp_years} năm`}</span>
                                </div>
                              </Col>
                            ))}
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Kỹ năng mềm</h4>
                          {candidate.soft_skills &&
                            candidate.soft_skills.map((skill, index) => (
                              <Col lg="12" className="info-skill" key={index}>
                                <div>
                                  <img src={skill.avatar} />
                                  <span>{skill.name}</span>
                                  <div className="progress-bar">
                                    <ProgressBar
                                      variant="warning"
                                      now={skill.value}
                                    />
                                  </div>
                                </div>
                              </Col>
                            ))}
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Giáo dục</h4>
                          <TableContainer component={Paper}>
                            <Table
                              className={classes.table}
                              aria-label="simple table"
                            >
                              <TableHead>
                                <TableRow className="table-head">
                                  <TableCell>STT</TableCell>
                                  <TableCell align="left">Trình độ</TableCell>
                                  <TableCell align="left">Trường</TableCell>
                                  <TableCell align="left">
                                    Chuyên ngành
                                  </TableCell>
                                  <TableCell align="left">
                                    Năm hoàn thành
                                  </TableCell>
                                  <TableCell align="left">Mô tả</TableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {candidate.educations &&
                                  candidate.educations.map((edu, index) => (
                                    <TableRow key={edu.id}>
                                      <TableCell component="th" scope="row">
                                        {index + 1}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {edu.degree}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.school_name}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.major_name}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.graduated_year}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.description}
                                      </TableCell>
                                    </TableRow>
                                  ))}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Kinh nghiệm</h4>
                          <TableContainer component={Paper}>
                            <Table
                              className={classes.table}
                              aria-label="simple table"
                            >
                              <TableHead>
                                <TableRow className="table-head">
                                  <TableCell>STT</TableCell>
                                  <TableCell align="left">
                                    Ngày bắt đầu
                                  </TableCell>
                                  <TableCell align="left">
                                    Ngày kết thúc
                                  </TableCell>
                                  <TableCell align="left">
                                    Tên công ty
                                  </TableCell>
                                  <TableCell align="left">Vị trí</TableCell>
                                  <TableCell align="left">Mô tả</TableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {candidate.work_exps &&
                                  candidate.work_exps.map((edu, index) => (
                                    <TableRow key={edu.id}>
                                      <TableCell component="th" scope="row">
                                        {index + 1}
                                      </TableCell>
                                      <TableCell component="th" scope="row">
                                        {edu.start_date}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.end_date}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.company_name}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.position}
                                      </TableCell>
                                      <TableCell align="left">
                                        {edu.description}
                                      </TableCell>
                                    </TableRow>
                                  ))}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Ngoại ngữ</h4>
                          <div className="list-hobby-tag">
                            {candidate.languages &&
                              candidate.languages.map((lang, index) => (
                                <Col lg="12" className="info-skill" key={index}>
                                  <div>
                                    <img src={lang.avatar} />
                                    <span>{lang.name}</span>
                                    <div className="progress-bar">
                                      <ProgressBar
                                        variant="warning"
                                        now={lang.value}
                                      />
                                    </div>
                                  </div>
                                </Col>
                              ))}
                          </div>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Sở thích</h4>
                          <div className="list-hobby-tag">
                            {candidate.hobbies &&
                              candidate.hobbies.map((hob, index) => (
                                <Col lg="12" className="info-skill" key={index}>
                                  <div>
                                    <img src={hob.avatar} />
                                    <span>{hob.name}</span>
                                  </div>
                                </Col>
                              ))}
                          </div>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content subject-skill"
                        >
                          <h4 className="main-title">Lời giới thiệu</h4>
                          <div className="list-hobby-tag">
                            {candidate.introduce}
                          </div>
                        </Col>

                        <Col
                          lg="12"
                          className="container-content optional-info"
                        >
                          <h4 className="main-title">thông tin thêm</h4>
                          <Row style={{ margin: '0' }}>
                            <Col lg="6" className="sub-optional-info">
                              <h6>Địa điểm mong muốn làm việc: </h6>
                              <span>{candidate.desire_location_of_work}</span>
                            </Col>
                            <Col lg="6" className="sub-optional-info">
                              <h6>Loại hình công việc mong muốn: </h6>
                              <span>{candidate.desire_type_of_work}</span>
                            </Col>

                            <Col lg="6" className="sub-optional-info">
                              <h6>Vị trí mong muốn</h6>
                              <span>{candidate.desire_job_position}</span>
                            </Col>
                            <Col lg="6" className="sub-optional-info">
                              <h6>Trạng thái</h6>
                              {candidate.status === 1 ? (
                                <span>Sẵn sàng tìm việc</span>
                              ) : (
                                <span>Không có nhu cầu tìm việc</span>
                              )}
                            </Col>

                            <Col lg="6" className="sub-optional-info">
                              <h6>Mức lương hiện tại</h6>
                              <span>{formatter.format(candidate.salary)}</span>
                            </Col>
                            <Col lg="6" className="sub-optional-info">
                              <h6>Mức lương mong muốn</h6>
                              <span>
                                {formatter.format(candidate.desire_salary)}
                              </span>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                  <Col lg="12" style={{ textAlign: 'center', margin: 10 }}>
                    <div className="action">
                      <Button
                        onClick={handleApprove}
                        style={{ marginRight: 30 }}
                        variant="contained"
                        color="primary"
                        size="small"
                      >
                        Chấp thuận
                      </Button>
                      <Button
                        style={{ backgroundColor: 'rgb(220, 0, 78)' }}
                        variant="contained"
                        color="secondary"
                        size="small"
                        onClick={handleReject}
                      >
                        Từ chối
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        )}
      </div>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  candidate: state.profileCandidate,
  post: state.posts,
});

const mapDispatchToProps = (dispatch) => {
  return {
    reject: (userId, postId) => {
      dispatch(rejectCV(userId, postId));
    },
    approve: (userId, postId) => {
      dispatch(approveCV(userId, postId));
    },
  };
};

CandidateDetail.propTypes = {
  candidate: PropTypes.instanceOf(Object),
  post: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object),
  history: PropTypes.instanceOf(Object),
  reject: PropTypes.func,
  approve: PropTypes.func,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(CandidateDetail);
