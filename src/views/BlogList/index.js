/* eslint-disable react/no-array-index-key */
/* eslint-disable react/require-default-props */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Pagination from '@material-ui/lab/Pagination';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import {
  fetchPostByidCompany,
  deletePostItem,
} from '../../Actions/post.actions';
import BlogItem from '../../Components/BlogList/BlogItem';
import BlogList from '../../Components/BlogList';

const styles = {
  pagination: {
    marginTop: 6,
    display: 'flex',
    justifyContent: 'center',
  },
  addBtn: {
    position: 'fixed',
    bottom: 10,
    right: 10,
  },
};

const BlogListPage = ({ posts, fetchPost, deletePost, classes }) => {
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const id = localStorage.getItem('user_id');
    fetchPost(id, 'BLOG', offset);
  }, [offset]);

  const onDelete = (postId) => {
    deletePost(postId);
  };

  const showBlogItem = (data) => {
    return Array.isArray(data) &&
      data.length > 0 &&
      data[0].postType === 'BLOG' ? (
      data.map((post, index) => (
        <BlogItem
          key={index}
          post={post}
          index={index + 1}
          onDelete={onDelete}
        />
      ))
    ) : (
      <tr style={{ textAlign: 'center' }}>
        <td colSpan="7">Chưa có bài đăng nào.</td>
      </tr>
    );
  };

  const handleChangePage = (event, value) => {
    setOffset((value - 1) * 10);
  };

  return (
    <>
      <BlogList>{showBlogItem(posts.data)}</BlogList>
      <div className={classes.pagination}>
        <Pagination
          count={Math.ceil(posts.total / 10)}
          onChange={handleChangePage}
        />
      </div>
      <Link to="/post-blog">
        <Fab className={classes.addBtn} color="primary" aria-label="add">
          <AddIcon />
        </Fab>
      </Link>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPost: (idCompany, postType) => {
      dispatch(fetchPostByidCompany(idCompany, postType));
    },
    deletePost: (postId) => {
      dispatch(deletePostItem(postId));
    },
  };
};

BlogListPage.propTypes = {
  posts: PropTypes.instanceOf(Object),
  fetchPost: PropTypes.func,
  deletePost: PropTypes.func,
  classes: PropTypes.instanceOf(Object),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(BlogListPage);
