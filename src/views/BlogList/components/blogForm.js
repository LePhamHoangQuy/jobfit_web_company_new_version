import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import Blog from '../../Blog';

const styles = {
  appBar: {
    position: 'relative',
  },
};

const BlogForm = ({ setOpenForm, open, classes }) => {
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => setOpenForm(false)}
        aria-labelledby="form-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={() => setOpenForm(false)}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent>
          <Blog />
        </DialogContent>
      </Dialog>
    </div>
  );
};

BlogForm.propTypes = {};

export default withStyles(styles)(BlogForm);
