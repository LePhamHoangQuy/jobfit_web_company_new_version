/* eslint-disable react/no-array-index-key */
/* eslint-disable react/require-default-props */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import { getCandidateFollow, getProfile } from '../../Actions/candidate';
import CandidateItem from './components/CandidateItem';
import CandidateList from './components';

const styles = {
  pagination: {
    marginTop: 6,
    display: 'flex',
    justifyContent: 'center',
  },
};

const Candidate = ({ getCandidate, candidates, getProfileCandidate }) => {
  useEffect(() => {
    getCandidate();
  }, []);

  const showCandidateItem = (data) => {
    return Array.isArray(data) && data.length > 0 ? (
      data.map((candidate, index) => (
        <CandidateItem
          getProfileCandidate={getProfileCandidate}
          key={index}
          candidate={candidate}
          index={index + 1}
        />
      ))
    ) : (
      <tr style={{ textAlign: 'center' }}>
        <td colSpan="12">Chưa có ứng viên nào.</td>
      </tr>
    );
  };

  // const handleChangePage = (event, value) => {
  //   setOffset((value - 1) * 10);
  // };

  return (
    <>
      <CandidateList>{showCandidateItem(candidates.data)}</CandidateList>
      {/* <div className={classes.pagination}>
        <Pagination
          count={Math.ceil(candidates.total / 10)}
          onChange={handleChangePage}
        />
      </div> */}
    </>
  );
};

const mapStateToProps = (state) => ({
  candidates: state.candidates,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidate: (id, offset) => {
      dispatch(getCandidateFollow(id, offset));
    },
    getProfileCandidate: (id) => {
      dispatch(getProfile(id));
    },
  };
};

Candidate.propTypes = {
  getCandidate: PropTypes.func,
  getProfileCandidate: PropTypes.func,
  candidates: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(Candidate);
