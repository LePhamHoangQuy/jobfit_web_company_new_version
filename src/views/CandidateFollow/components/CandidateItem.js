/* eslint-disable react/require-default-props */
/* eslint-disable default-case */
import React from 'react';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Dropdown from 'react-bootstrap/Dropdown';
import { withStyles } from '@material-ui/core/styles';
// import CandidateDetail from "../../Dashboard/CandidateDetail";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const styles = {
  btn: {
    marginRight: 44,
  },
};

const CandidateItem = ({ candidate, index, getProfileCandidate }) => {
  const handleShowDetail = () => {
    getProfileCandidate(candidate.id);
  };

  return (
    <>
      <tr>
        <td>{index}</td>
        <td>{candidate.username}</td>
        <td>{candidate.email}</td>
        <td>
          <Dropdown>
            <Dropdown.Toggle variant="danger" id="dropdown-basic">
              <FormatListBulletedIcon />
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>
                <Link onClick={handleShowDetail} to="/candidate-detail">
                  Xem chi tiết
                </Link>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </td>
      </tr>
    </>
  );
};

CandidateItem.propTypes = {
  candidate: PropTypes.instanceOf(Object),
  index: PropTypes.number,
  getProfileCandidate: PropTypes.func,
};

export default withStyles(styles)(CandidateItem);
