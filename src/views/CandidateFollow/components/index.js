import React from 'react';
import Table from 'react-bootstrap/Table';
import { Row, Col } from 'react-bootstrap';
import Filter from './Filter';

const CandidateList = ({ children }) => {
  return (
    <div className="candidate-list">
      <Row>
        <Col lg="12">
          <div className="title text-center" style={{ padding: '15px' }}>
            <h3>DANH SÁCH ỨNG VIÊN</h3>
            <h6 style={{ color: 'gray' }}>
              (Danh sách những ứng viên đang theo dõi)
            </h6>
          </div>
        </Col>
        <Col lg="12">
          <Filter />
        </Col>
        <Col lg="12">
          <Table
            className="table-post"
            responsive="lg"
            striped
            borderless
            hover
            variant="light"
          >
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên ứng viên</th>
                <th>Email</th>
                <th>Thao tác</th>
              </tr>
            </thead>
            <tbody>{children}</tbody>
          </Table>
        </Col>
      </Row>
    </div>
  );
};

export default CandidateList;
