/* eslint-disable react/require-default-props */
/* eslint-disable react/destructuring-assignment */
import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import 'react-select2-wrapper/css/select2.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faClock,
  faDollarSign,
  faShare,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import Button from 'react-bootstrap/Button';
import ButtonMate from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import _ from 'lodash';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { reset } from 'redux-form';
import { formatter } from '../../Helpers/ToCurrency';
import { createMarkup } from '../../Helpers/ToHTML';
import {
  setInputUpdate,
  getCommentById,
  setComment,
} from '../../Actions/post.actions';
import Comment from './components/comment';

const styles = {
  back: {
    position: 'relative',
    top: 50,
    zIndex: 100,
    '& a': {
      textDecoration: 'none',
    },
  },
};

const PostDetail = (props) => {
  const { aboutProps } =
    props.location.aboutProps !== undefined && props.location;
  const { post } = aboutProps !== undefined && aboutProps;
  const {
    classes,
    setInput,
    getComment,
    comment,
    setCommentByCompany,
    resetForm,
    profile,
  } = props;

  const dueDate = new Date(_.get(post, 'dueDate', ''));
  const locations = _.get(post, 'locations', []);
  const salary = _.get(post, 'salary', '');

  const handleSetInput = () => {
    const values = {
      id: post.id,
      name: post.name,
      jobCategory: post.jobCategory,
      jobType: post.jobType,
      amount: post.amount,
      addressName: post.locations[0].addressName,
      description: post.description,
      experiment: post.experiment,
      jobRequirement: post.jobRequirement,
      jobBenefit: post.jobBenefit,
      gender: post.gender,
      salary: post.salary,
      dueDate,
      position: post.position,
    };
    setInput(values);
  };

  useEffect(() => {
    if (!_.isEmpty(post)) getComment(post.postId);
    resetForm();
  }, []);

  return (
    <Container style={{ marginTop: '0px' }}>
      {aboutProps ? (
        <>
          <div className={classes.back}>
            <Link to="/post-list">
              <ButtonMate color="primary">
                <KeyboardBackspaceIcon />
              </ButtonMate>
            </Link>
          </div>
          <div className="post-detail">
            <Row>
              <Container>
                <Col lg="12">
                  <Row style={{ padding: '15px 15px 15px 60px' }}>
                    <Col lg="11">
                      <div
                        className="title text-left"
                        style={{ padding: '0px' }}
                      >
                        <h3>CHI TIẾT BÀI ĐĂNG</h3>

                        <h6 style={{ color: 'gray' }}>
                          (Chi tiết bài đăng tin của nhà tuyển dụng)
                        </h6>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col lg="12" className="post-detail-body">
                  <Row>
                    <Col lg="12">
                      <h5 className="post-name">{_.get(post, 'name', '')}</h5>
                    </Col>
                    <Col lg="6" sm="8" style={{ marginTop: '5px' }}>
                      <h6>
                        <FontAwesomeIcon icon={faClock} /> Ngày hết hạn:{' '}
                        <span className="expired-date">
                          {dueDate.toLocaleDateString()}
                        </span>{' '}
                      </h6>
                    </Col>
                    <Col lg="6" sm="8" style={{ marginTop: '5px' }}>
                      <h6>
                        {' '}
                        <FontAwesomeIcon icon={faDollarSign} /> Mức lương:{' '}
                        <span className="expired-date">
                          {salary ? formatter.format(salary) : 'Thỏa thuận'}
                        </span>
                      </h6>
                    </Col>
                    <Col lg="12" sm="12" style={{ marginTop: '5px' }}>
                      <Button variant="outline-info">
                        <FontAwesomeIcon icon={faShare} /> Chia sẻ
                      </Button>{' '}
                      <Link to="/post-job">
                        <Button
                          variant="outline-danger"
                          onClick={handleSetInput}
                        >
                          <FontAwesomeIcon icon={faEdit} /> Chỉnh sửa bài viết
                        </Button>{' '}
                      </Link>
                    </Col>
                    <Col
                      lg="12"
                      className="group-title"
                      style={{ marginTop: '20px' }}
                    >
                      <h5 className="title-post">Mô tả công việc</h5>
                      <div
                        dangerouslySetInnerHTML={createMarkup(
                          _.get(post, 'description', ''),
                        )}
                      />
                    </Col>

                    <Col
                      lg="12"
                      className="group-title"
                      style={{ marginTop: '20px' }}
                    >
                      <h5 className="title-post">Yêu cầu công việc</h5>
                      <div
                        dangerouslySetInnerHTML={createMarkup(
                          _.get(post, 'experiment', ''),
                        )}
                      />
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Yêu cầu kinh nghiệm</h5>
                      <div
                        dangerouslySetInnerHTML={createMarkup(
                          _.get(post, 'experiment', ''),
                        )}
                      />
                    </Col>
                    <Col
                      lg="12"
                      className="group-title"
                      style={{ marginTop: '20px' }}
                    >
                      <h5 className="title-post">Quyền lợi được hưởng</h5>
                      <div
                        dangerouslySetInnerHTML={createMarkup(
                          _.get(post, 'jobBenefit', ''),
                        )}
                      />
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Địa chỉ làm việc</h5>
                      <span>
                        {' '}
                        {locations.length > 0 &&
                          _.get(locations[0], 'addressName', '')}
                      </span>
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Yêu cầu bằng cấp</h5>
                      <span>Đại học</span>
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Ngành nghề</h5>
                      {post.jobCategory &&
                        post.jobCategory.map((category) => (
                          <span key={category} className="job-category-tag">
                            {category}
                          </span>
                        ))}
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Hình thức công việc</h5>
                      <span className="job-type-tag">
                        {_.get(post, 'jobType', '')}
                      </span>
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Giới tính</h5>
                      <span className="job-gender-tag">
                        {_.get(post, 'gender')}
                      </span>
                    </Col>
                    <Col lg="12" className="group-title">
                      <h5 className="title-post">Số lượng</h5>
                      <span>
                        {post.amount
                          ? `${post.amount} người`
                          : 'Không giới hạn'}
                      </span>
                    </Col>
                  </Row>
                </Col>
              </Container>
            </Row>
            <Comment
              comment={comment}
              post={post}
              setCommentByCompany={setCommentByCompany}
              profile={profile}
            />
          </div>
        </>
      ) : (
        <div>Không tìm thấy.</div>
      )}
    </Container>
  );
};

const mapStateToProps = (state) => ({
  comment: state.comment,
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setInput: (values) => {
      dispatch(setInputUpdate(values));
    },
    getComment: (id) => {
      dispatch(getCommentById(id));
    },
    setCommentByCompany: (data) => {
      dispatch(setComment(data));
    },
    resetForm: () => {
      dispatch(reset('commentForm'));
    },
  };
};

PostDetail.propTypes = {
  location: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object),
  setInput: PropTypes.func,
  getComment: PropTypes.func,
  setCommentByCompany: PropTypes.func,
  comment: PropTypes.instanceOf(Array),
  profile: PropTypes.instanceOf(Object),
  resetForm: PropTypes.func,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(PostDetail);
