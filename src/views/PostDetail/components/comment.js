import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CommentItem from './commentItem';
import CommentFormm from './commentForm';

const styles = {
  commentContainer: {
    padding: 14,
  },
  form: {
    marginBottom: 10,
    borderBottom: '1px solid #eee',
    paddingBottom: 20,
  },
};

const Comment = ({ classes, comment, post, setCommentByCompany, profile }) => {
  const submit = (values) => {
    const profile = JSON.parse(localStorage.getItem('profile'));
    const data = {
      postId: post.postId,
      userId: profile.id.toString(),
      content: values.content,
      avatar: profile.user_info.avatar,
      fullName: profile.username,
    };
    setCommentByCompany(data);
  };
  return (
    <div className={classes.commentContainer}>
      <h5 className={classes.title}>Bình luận bài đăng</h5>
      <div className={classes.form}>
        <CommentFormm onSubmit={submit} profile={profile} />
      </div>
      {comment.length > 0 &&
        comment.map((item) => <CommentItem comment={item} key={item.id} />)}
    </div>
  );
};

Comment.propTypes = {
  classes: PropTypes.instanceOf(Object),
  comment: PropTypes.instanceOf(Array),
  post: PropTypes.instanceOf(Array),
  setCommentByCompany: PropTypes.func,
  profile: PropTypes.instanceOf(Object),
};

export default withStyles(styles)(Comment);
