import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';

const styles = {
  comment: {
    borderBottom: '1px solid #eee',
    marginBottom: 10,
    paddingBottom: 10,
  },
  commentInfo: {
    display: 'flex',
  },
  avatar: {
    width: 39,
    '& img': {
      width: '70%',
      borderRadius: '50%',
    },
  },
  name: {
    fontWeight: 'bold',
    marginRight: 10,
  },
  time: {
    color: '#666',
    fontSize: 11,
    marginTop: 5,
  },
  content: {
    marginLeft: 40,
    marginTop: 10,
  },
};

const CommentItem = ({ classes, comment }) => {
  const createdTime = new Date(_.get(comment, 'createdTime', ''));
  const time = moment(createdTime).startOf().fromNow();
  return (
    <div className={classes.comment}>
      <div className={classes.commentInfo}>
        <div className={classes.avatar}>
          <img src={comment.avatar} alt="avatar" />
        </div>
        <div className={classes.name}>{comment.fullName}</div>
        <div className={classes.time}>{time}</div>
      </div>
      <div className={classes.content}>{comment.content}</div>
    </div>
  );
};

CommentItem.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  comment: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(CommentItem);
