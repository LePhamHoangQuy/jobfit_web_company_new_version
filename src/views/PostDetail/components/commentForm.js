/* eslint-disable react/require-default-props */
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '../../../Components/CustomField/TextField';

const styles = {
  commentInfo: {
    display: 'flex',
  },
  avatar: {
    width: 39,
    '& img': {
      width: '70%',
      borderRadius: '50%',
    },
  },
  name: {
    fontWeight: 'bold',
    marginRight: 10,
  },
  field: {
    margin: '10px 0',
    '& div': {
      width: '50%',
    },
  },
  btn: {
    fontSize: 12,
  },
};

let CommentForm = ({ handleSubmit, classes, profile }) => {
  return (
    <div>
      <div className={classes.commentInfo}>
        <div className={classes.avatar}>
          <img alt="avatar" src={profile.user_info.avatar} />
        </div>
        <div className={classes.name}>{profile.username}</div>
      </div>
      <form onSubmit={handleSubmit} className={classes.Form}>
        <div className={classes.field}>
          <Field
            name="content"
            component={TextField}
            label="Nhập bình luận của bạn"
            margin="dense"
            required
            variant="outlined"
            rowsMax={6}
            multiline
          />
        </div>
        <div className={classes.btn}>
          <Button
            type="submit"
            color="primary"
            variant="contained"
            size="small"
          >
            Bình luận
          </Button>
        </div>
      </form>
    </div>
  );
};

CommentForm = reduxForm({
  form: 'commentForm',
  destroyOnUnmount: false,
})(CommentForm);

CommentForm.propTypes = {
  classes: PropTypes.instanceOf(Object),
  handleSubmit: PropTypes.func,
};

export default withStyles(styles)(CommentForm);
