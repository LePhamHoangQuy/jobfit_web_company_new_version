/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/require-default-props */
/* eslint-disable react/no-danger */
import React, { useEffect } from 'react';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import { createMarkup } from '../../Helpers/ToHTML';
import {
  setInputUpdate,
  getCommentById,
  setComment,
} from '../../Actions/post.actions';
import Comment from '../PostDetail/components/comment';

const styles = {
  back: {
    position: 'relative',
    top: 40,
    right: 60,
    zIndex: 100,
    '& a': {
      textDecoration: 'none',
    },
  },
};

const BlogDetail = (props) => {
  const {
    classes,
    resetForm,
    getComment,
    setCommentByCompany,
    comment,
    profile,
  } = props;
  const { aboutProps } =
    props.location.aboutProps !== undefined && props.location;
  const { post } = aboutProps !== undefined && aboutProps;

  useEffect(() => {
    if (!_.isEmpty(post)) getComment(post.postId);
    resetForm();
  }, []);

  return (
    <div style={{ padding: 60 }}>
      <div className={classes.back}>
        <Button color="primary" onClick={() => props.history.goBack()}>
          <KeyboardBackspaceIcon />
        </Button>
      </div>
      {post !== undefined && (
        <div
          dangerouslySetInnerHTML={createMarkup(_.get(post, 'description', ''))}
        />
      )}
      <Comment
        comment={comment}
        post={post}
        setCommentByCompany={setCommentByCompany}
        profile={profile}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  comment: state.comment,
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setInput: (values) => {
      dispatch(setInputUpdate(values));
    },
    getComment: (id) => {
      dispatch(getCommentById(id));
    },
    setCommentByCompany: (data) => {
      dispatch(setComment(data));
    },
    resetForm: () => {
      dispatch(reset('commentForm'));
    },
  };
};

BlogDetail.propTypes = {
  location: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object),
  getComment: PropTypes.func,
  setCommentByCompany: PropTypes.func,
  comment: PropTypes.instanceOf(Array),
  profile: PropTypes.instanceOf(Object),
  resetForm: PropTypes.func,
  history: PropTypes.instanceOf(Object),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(BlogDetail);
