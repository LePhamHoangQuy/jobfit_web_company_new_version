const validate = (values) => {
  const errors = {};
  const requiredFields = [
    'name',
    'position',
    'jobCategory',
    'jobType',
    'amount',
    'addressName',
    'description',
    'experiment',
    'dueDate',
    'gender',
    'salary',
  ];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Vui lòng điền thông tin này';
    }
  });
  return errors;
};

export default validate;
