/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography, Paper, Stepper, Step, StepLabel } from '@material-ui/core';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import compose from 'recompose/compose';
import _ from 'lodash';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Button from '@material-ui/core/Button';
import PostForm from './components/PostForm';
import PostReview from './components/PostReview';
import {
  createPost,
  fetchJobCategory,
  getJobPosition,
} from '../../Actions/post.actions';
import {
  fetchProvince,
  fetchDistrict,
  fetchWard,
} from '../../Actions/city.actions';
const styles = {
  layout: {
    width: 720,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  paper: {
    padding: 24,
    marginTop: 48,
    marginBottom: 48,
  },
  stepper: {
    padding: '24px 24px 40px',
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: '24px 0 0 8px',
  },
  back: {
    position: 'relative',
    top: 80,
    right: 100,
    zIndex: 100,
    '& a': {
      textDecoration: 'none',
    },
  },
};

const steps = ['Chi tiết bài đăng', 'Xem lại bài đăng'];

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      data: {},
      jobCategories: [],
    };
  }

  componentDidMount() {
    this.props.getJobCategory();
    this.props.fetchJobPosition();
  }

  handleNext = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep + 1,
    });
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep - 1,
    });
  };

  submitForm = (values) => {
    this.setState({
      data: values,
    });
    this.handleNext();
  };

  submitPostForm = (data) => {
    const { ward, district, province, apartmentNumber } = data;
    const dueDate = data.dueDate ? new Date(data.dueDate) : Date.now();
    let address;
    if (ward && district && province) {
      address = `${apartmentNumber},${ward},${district},${province}`;
    } else {
      address = null;
    }
    const locations = [];
    locations.push({
      addressName: address,
      lat: null,
      lng: null,
      districtName: district,
      wardName: ward,
      provinceName: province,
    });
    const companyId = localStorage.getItem('user_id');
    const companyName = localStorage.getItem('username');
    const profile = JSON.parse(localStorage.getItem('profile'));
    const body = {
      ..._.omit(data, ['addressName', 'dueDate']),
      locations,
      dueDate,
      companyId,
      companyName,
      companyAvatar: profile && profile.user_info.avatar,
      postType: 'JOB',
      likeCount: null,
    };
    this.props.createPost(body);
    this.props.reset();
  };

  getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <PostForm
            initialValues={this.props.inputUpdate}
            jobCategory={this.props.jobCategory}
            jobPosition={this.props.jobPosition}
            onSubmit={this.submitForm}
            places={this.props.places}
            getProvince={this.props.getProvince}
            getDistrict={this.props.getDistrict}
            getWard={this.props.getWard}
          />
        );
      case 1:
        return (
          <PostReview
            initialValues={this.props.inputUpdate}
            submitPostForm={this.submitPostForm}
            handleBack={this.handleBack}
            handleNext={this.handleNext}
            data={this.state.data}
          />
        );
      default:
        throw new Error('Unknown step');
    }
  };

  render() {
    const { classes } = this.props;
    const { activeStep } = this.state;
    return (
      <>
        <main className={classes.layout}>
          <div className={classes.back}>
            <Button color="primary" onClick={() => this.props.history.goBack()}>
              <KeyboardBackspaceIcon />
            </Button>
          </div>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
              Đăng bài
            </Typography>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <>
              {activeStep === steps.length ? (
                <>
                  <Typography variant="h5" gutterBottom>
                    Cảm ơn bạn đã đăng tin tuyển dụng.
                  </Typography>
                  <Typography variant="subtitle1">
                    Chúc bạn sẽ tìm được ứng viên mong muốn
                  </Typography>
                </>
              ) : (
                <>{this.getStepContent(activeStep)}</>
              )}
            </>
          </Paper>
        </main>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  jobCategory: state.jobCategory,
  inputUpdate: state.inputUpdatePost,
  jobPosition: state.jobPosition,
  places: state.places,
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchJobPosition: () => {
      dispatch(getJobPosition());
    },
    createPost: (body) => {
      dispatch(createPost(body));
    },
    getJobCategory: () => {
      dispatch(fetchJobCategory());
    },
    reset: () => {
      dispatch(reset('postForm'));
    },
    getProvince: () => {
      dispatch(fetchProvince());
    },
    getDistrict: (provinceId) => {
      dispatch(fetchDistrict(provinceId));
    },
    getWard: (districtId) => {
      dispatch(fetchWard(districtId));
    },
  };
};

Post.propTypes = {
  classes: PropTypes.instanceOf(Object),
  createPost: PropTypes.func,
  getJobCategory: PropTypes.func,
  reset: PropTypes.func,
  jobCategory: PropTypes.instanceOf(Array),
  history: PropTypes.instanceOf(Object),
  inputUpdate: PropTypes.instanceOf(Object),
  fetchJobPosition: PropTypes.func,
  jobPosition: PropTypes.instanceOf(Array),
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(Post);
