/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { renderSelectField } from '../../../Components/CustomField/SelectField';
import { renderTextField } from '../../../Components/CustomField/Field';
import EditorField from '../../../Components/CustomField/Editor';
import muiltiSelect from '../../../Components/CustomField/Multiselect';

const styles = {
  Form: {
    '& div': {
      marginBottom: 4,
    },
  },
  jobType: {
    textAlign: 'center',
  },
  gender: {
    textAlign: 'center',
    '& div': {
      minWidth: 85,
    },
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: '24px 0 0 8px',
  },
  label: {
    fontWeight: 'bold',
  },
};

let PostForm = (props) => {
  const {
    handleSubmit,
    classes,
    jobCategory,
    jobPosition,
    places,
    getProvince,
    getDistrict,
    getWard,
  } = props;

  useEffect(() => {
    getProvince();
  }, []);

  return (
    <form onSubmit={handleSubmit} className={classes.Form}>
      <div>
        <label className={classes.label}>Tên công việc</label>
        <Field name="name" component={renderTextField} fullWidth required />
      </div>

      <div>
        <Field
          name="position"
          label="Vị trí công việc"
          component={renderSelectField}
        >
          <option value="" disabled />
          {Array.isArray(jobPosition) &&
            jobPosition.map(({ name }) => (
              <option key={name} value={name}>
                {name}
              </option>
            ))}
        </Field>
      </div>
      <div>
        <Field
          name="jobCategory"
          label="Ngành nghề"
          component={muiltiSelect}
          contents={jobCategory}
          property="skillName"
          required
        />
      </div>
      <div>
        <Grid container spacing={3}>
          <Grid item xs>
            <div>
              <Field
                name="jobType"
                label="Hình thức"
                component={renderSelectField}
                required
              >
                <option value="" disabled />
                <option value="FULLTIME">Full - time</option>
                <option value="PARTTIME">Part - time</option>
              </Field>
            </div>
          </Grid>
          <Grid item xs>
            <div>
              <Field
                name="amount"
                type="number"
                component={renderTextField}
                label="Số lượng"
                required
              />
            </div>
          </Grid>
        </Grid>
      </div>
      <div className="field">
        <label style={{ fontWeight: 'bold' }}>Địa chỉ</label>
        <div className="addressOption">
          <Grid container spacing={3}>
            <Grid item xs>
              <div>
                <Field
                  required
                  name="apartmentNumber"
                  component={renderTextField}
                  fullWidth
                  label="Số nhà"
                />
              </div>
            </Grid>
            <Grid item xs>
              <div className="item">
                <Field
                  required
                  name="province"
                  label="Tỉnh thành"
                  component={renderSelectField}
                  onChange={(e) => {
                    var select = document.getElementsByName('province')[0];
                    var options = select.options;
                    var id = options[options.selectedIndex].id;
                    return getDistrict(id);
                  }}
                >
                  <option value="" disabled />
                  {places.provinces &&
                    places.provinces.map((place, index) => (
                      <option
                        id={place.provinceId}
                        key={index}
                        value={place.provinceName}
                      >
                        {place.provinceName}
                      </option>
                    ))}
                </Field>
              </div>
            </Grid>
            <Grid item xs>
              <div className="item">
                <Field
                  required
                  style={{ minWidth: 100 }}
                  name="district"
                  label="Huyện"
                  component={renderSelectField}
                  onChange={(e) => {
                    var select = document.getElementsByName('district')[0];
                    var options = select.options;
                    var id = options[options.selectedIndex].id;
                    return getWard(id);
                  }}
                >
                  <option value="" disabled />
                  {places.districts &&
                    places.districts.map((place, index) => (
                      <option
                        key={index}
                        id={place.districtId}
                        value={place.districtName}
                      >
                        {place.districtName}
                      </option>
                    ))}
                </Field>
              </div>
            </Grid>
            <Grid item xs>
              <div className="item">
                <Field
                  required
                  style={{ minWidth: 100 }}
                  name="ward"
                  label="Phường, xã"
                  component={renderSelectField}
                >
                  <option value="" disabled />
                  {places.wards &&
                    places.wards.map((place, index) => (
                      <option key={index} value={place.wardName}>
                        {place.wardName}
                      </option>
                    ))}
                </Field>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      <div>
        <label className={classes.label}>Mô tả công việc</label>
        <EditorField
          key="field"
          name="description"
          id="inputEditorText"
          disabled={false}
          placeholder="Type here"
        />
      </div>
      <div>
        <label className={classes.label}>Kinh nghiệm</label>
        <EditorField
          key="field"
          name="experiment"
          id="inputEditorText"
          disabled={false}
          placeholder="Type here"
        />
      </div>
      <div>
        <label className={classes.label}>Yêu cầu công việc</label>
        <EditorField
          key="field"
          name="jobRequirement"
          id="inputEditorText"
          disabled={false}
          placeholder="Type here"
        />
      </div>
      <div>
        <label className={classes.label}>Phúc lợi</label>
        <EditorField
          key="field"
          name="jobBenefit"
          id="inputEditorText"
          disabled={false}
          placeholder="Type here"
        />
      </div>
      <div>
        <Grid container spacing={3}>
          <Grid item xs>
            <div>
              <Field
                name="dueDate"
                component={renderTextField}
                type="date"
                label="Ngày hết hạng"
                required
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </Grid>
          <Grid item xs>
            <div className={classes.gender}>
              <Field
                name="gender"
                required
                label="Giới tính"
                component={renderSelectField}
              >
                <option value="Tất cả">Tất cả</option>
                <option value="Nam">Nam</option>
                <option value="Nữ">Nữ</option>
              </Field>
            </div>
          </Grid>
          <Grid item xs>
            <div>
              <Field
                required
                name="salary"
                component={renderTextField}
                label="Mức lương"
              />
            </div>
          </Grid>
        </Grid>
      </div>
      <div className={classes.buttons}>
        <Button
          type="submit"
          className={classes.button}
          variant="contained"
          color="primary"
        >
          Tiếp theo
        </Button>
      </div>
    </form>
  );
};

PostForm = reduxForm({
  form: 'postForm',
  destroyOnUnmount: false,
  // validate,
})(PostForm);

PostForm.propTypes = {
  handleSubmit: PropTypes.func,
  classes: PropTypes.instanceOf(Object),
  jobCategory: PropTypes.instanceOf(Array),
  jobPosition: PropTypes.instanceOf(Array),
};

export default compose(withStyles(styles), connect(null, null))(PostForm);
