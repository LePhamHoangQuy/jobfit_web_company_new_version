/* eslint-disable react/require-default-props */
import React from 'react';
import _ from 'lodash';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { formatter } from '../../../Helpers/ToCurrency';
import { createMarkup } from '../../../Helpers/ToHTML';

const styles = {
  center: {
    textAlign: 'center',
    '& span': {
      fontWeight: 'bold',
    },
  },
  left: {
    '& > span': {
      fontWeight: 'bold',
    },
  },
  jobType: {
    marginLeft: 70,
    '& span': {
      fontWeight: 'bold',
    },
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: '24px 0 0 8px',
  },
};

const PostReview = ({
  data,
  classes,
  handleBack,
  submitPostForm,
  handleNext,
  initialValues,
}) => {
  const { ward, district, province, apartmentNumber } = data;
  let address;
  if (ward && district && province) {
    address = `${apartmentNumber},${ward},${district},${province}`;
  } else {
    address = null;
  }
  return (
    <>
      <List className={classes.list}>
        <ListItem>
          <ListItemText
            className={classes.center}
            primary="Tên công việc"
            secondary={_.get(data, 'name', '')}
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.center}
            primary="Vị trí"
            secondary={_.get(data, 'position', '')}
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.center}
            primary="Ngành nghề"
            secondary={_.get(data, 'jobCategory', '')}
          />
          <ListItemText
            className={classes.jobType}
            primary="Hình thức"
            secondary={_.get(data, 'jobType', '')}
          />
          <ListItemText
            className={classes.center}
            primary="Số lượng"
            secondary={_.get(data, 'amount', '')}
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.left}
            primary="Địa chỉ làm việc"
            secondary={address}
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.left}
            primary="Mô tả công việc"
            secondary={
              <div
                dangerouslySetInnerHTML={createMarkup(
                  _.get(data, 'description', ''),
                )}
              />
            }
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.left}
            primary="Kinh nghiệm"
            secondary={
              <div
                dangerouslySetInnerHTML={createMarkup(
                  _.get(data, 'experiment', ''),
                )}
              />
            }
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.left}
            primary="Yêu cầu công việc"
            secondary={
              <div
                dangerouslySetInnerHTML={createMarkup(
                  _.get(data, 'jobRequirement', ''),
                )}
              />
            }
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.left}
            primary="Phúc lợi"
            secondary={
              <div
                dangerouslySetInnerHTML={createMarkup(
                  _.get(data, 'jobBenefit', ''),
                )}
              />
            }
          />
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <ListItemText
            className={classes.center}
            primary="Ngày hết hạn"
            secondary={_.get(data, 'dueDate', '')}
          />
          <ListItemText
            className={classes.center}
            primary="Giới tính"
            secondary={_.get(data, 'gender', '')}
          />
          <ListItemText
            className={classes.center}
            primary="Mức lương"
            secondary={formatter.format(_.get(data, 'salary', ''))}
          />
        </ListItem>
        <Divider component="li" />
      </List>
      <div className={classes.buttons}>
        <Button onClick={() => handleBack()} className={classes.button}>
          Trở lại
        </Button>
        <Button
          onClick={() => {
            submitPostForm(data);
            handleNext();
          }}
          className={classes.button}
          variant="contained"
          color="primary"
        >
          {_.isEmpty(initialValues) ? `Đăng ngay` : `Cập nhật ngay`}
        </Button>
      </div>
    </>
  );
};

PostReview.propTypes = {
  classes: PropTypes.instanceOf(Object),
  data: PropTypes.instanceOf(Object),
  handleBack: PropTypes.func,
  submitPostForm: PropTypes.func,
  handleNext: PropTypes.func,
};

export default withStyles(styles)(PostReview);
