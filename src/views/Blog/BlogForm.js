/* eslint-disable react/require-default-props */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import EditIcon from '@material-ui/icons/Edit';
import 'react-select2-wrapper/css/select2.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Field, reduxForm } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import EditorField from '../../Components/CustomField/Editor';
import { renderTextField } from '../../Components/CustomField/Field';
import muiltiSelect from '../../Components/CustomField/Multiselect';

const styles = {
  Form: {
    '& div': {
      marginBottom: 4,
    },
  },
  jobType: {
    textAlign: 'center',
  },
  gender: {
    textAlign: 'center',
    '& div': {
      minWidth: 85,
    },
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: '24px 0 0 8px',
  },
  label: {
    fontWeight: 'bold',
  },
};

let BlogForm = (props) => {
  const { classes, handleSubmit, jobCategory, tags } = props;

  return (
    <Container className="blog-component">
      <Row>
        <Col lg="12 text-center blog-header">
          <h2>
            <EditIcon /> Form Đăng Blog{' '}
          </h2>
          <h6>
            (Trang giúp bạn chia sẻ những kinh nghiệm thông qua những bài blog)
          </h6>
        </Col>
        <Col className="blog-body" lg="12" style={{ marginTop: '30px' }}>
          <Row>
            <Col lg={{ span: 8, offset: 2 }} className="form-blog">
              <form onSubmit={handleSubmit} className={classes.Form}>
                <div>
                  <label className={classes.label}>Tên bài blog</label>
                  <Field
                    name="name"
                    component={renderTextField}
                    fullWidth
                    required
                  />
                </div>
                <div>
                  <Grid container spacing={3}>
                    <Grid item xs>
                      <div>
                        <Field
                          name="jobCategory"
                          label="Ngành nghề"
                          component={muiltiSelect}
                          contents={jobCategory}
                          property="skillName"
                          required
                        />
                      </div>
                    </Grid>
                    <Grid item xs>
                      <div>
                        <Field
                          name="tags"
                          label="Tags"
                          component={muiltiSelect}
                          contents={tags}
                          property="childSkillName"
                          required
                        />
                      </div>
                    </Grid>
                  </Grid>
                </div>
                <div>
                  <label className={classes.label}>Kinh nghiệm</label>
                  <EditorField
                    key="field"
                    name="description"
                    id="inputEditorText"
                    disabled={false}
                    placeholder="Type here"
                  />
                </div>
                <div className={classes.buttons}>
                  <Button
                    type="submit"
                    className={classes.button}
                    variant="contained"
                    color="primary"
                  >
                    Đăng bài
                  </Button>
                </div>
              </form>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

BlogForm = reduxForm({
  form: 'blogForm',
  destroyOnUnmount: false,
})(BlogForm);

BlogForm.propTypes = {
  handleSubmit: PropTypes.func,
  classes: PropTypes.instanceOf(Object),
  jobCategory: PropTypes.instanceOf(Array),
  tags: PropTypes.instanceOf(Array),
};

export default withStyles(styles)(BlogForm);
