/* eslint-disable react/require-default-props */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import ButtonMate from '@material-ui/core/Button';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import BlogForm from './BlogForm';
import {
  createPost,
  fetchJobCategory,
  getChildProSkill,
} from '../../Actions/post.actions';

const styles = {
  back: {
    position: 'relative',
    top: 80,
    right: 100,
    zIndex: 100,
    '& a': {
      textDecoration: 'none',
    },
  },
};
const Blog = ({
  createBlog,
  getJobCategory,
  jobCategory,
  classes,
  history,
  fetchTags,
  tags,
  resetForm,
}) => {
  useEffect(() => {
    getJobCategory();
    fetchTags();
  }, []);

  const submit = (values) => {
    const profile = JSON.parse(localStorage.getItem('profile'));

    const data = {
      name: values.name,
      jobCategory: values.jobCategory,
      companyId: profile.id.toString(),
      companyName: profile.username,
      companyAvatar: profile.user_info.avatar,
      description: values.description,
      postType: 'BLOG',
      tags: values.tags,
    };
    createBlog(data);
    resetForm();
  };
  return (
    <>
      <div className={classes.back}>
        <ButtonMate color="primary" onClick={() => history.goBack()}>
          <KeyboardBackspaceIcon />
        </ButtonMate>
      </div>
      <BlogForm onSubmit={submit} jobCategory={jobCategory} tags={tags} />
    </>
  );
};

const mapStateToProps = (state) => ({
  jobCategory: state.jobCategory,
  tags: state.tags,
});

const mapDispatchToProps = (dispatch) => {
  return {
    createBlog: (body) => {
      dispatch(createPost(body));
    },
    getJobCategory: () => {
      dispatch(fetchJobCategory());
    },
    resetForm: () => {
      dispatch(reset('blogForm'));
    },
    fetchTags: () => {
      dispatch(getChildProSkill());
    },
  };
};

Blog.propTypes = {
  classes: PropTypes.instanceOf(Object),
  history: PropTypes.instanceOf(Object),
  createBlog: PropTypes.func,
  getJobCategory: PropTypes.func,
  jobCategory: PropTypes.instanceOf(Array),
  tags: PropTypes.instanceOf(Array),
  fetchTags: PropTypes.func,
  resetForm: PropTypes.func,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(Blog);
