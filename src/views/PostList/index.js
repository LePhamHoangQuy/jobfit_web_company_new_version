import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Pagination from '@material-ui/lab/Pagination';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';
import { reset } from 'redux-form';
import {
  fetchPostByidCompany,
  deletePostItem,
  fetchJobCategory,
} from '../../Actions/post.actions';
import PostItem from './components/PostItem';
import PostList from './components/PostList';

const styles = {
  pagination: {
    marginTop: 6,
    display: 'flex',
    justifyContent: 'center',
  },
};

const PostListComponent = ({
  fetchPost,
  posts,
  classes,
  deletePost,
  getJobCategory,
  jobCategory,
  resetFilter,
}) => {
  const [offset, setOffset] = useState(0);
  const [jobType, setJobType] = useState();
  const [jobCategoryFiter, setJobCategory] = useState([]);

  useEffect(() => {
    const id = localStorage.getItem('user_id');
    fetchPost(id, 'JOB', offset, jobType, jobCategoryFiter);
  }, [offset, jobType, jobCategoryFiter]);

  useEffect(() => {
    getJobCategory();
  }, []);

  const onDelete = (postId) => {
    deletePost(postId);
  };

  const showPostItem = (data) => {
    return Array.isArray(data) &&
      data.length > 0 &&
      data[0].postType === 'JOB' ? (
      data.map((post, index) => (
        <PostItem
          key={post.id}
          post={post}
          onDelete={onDelete}
          index={index + 1}
        />
      ))
    ) : (
      <tr style={{ textAlign: 'center' }}>
        <td colSpan="12">Chưa có bài đăng nào.</td>
      </tr>
    );
  };

  const handleChangePage = (event, value) => {
    setOffset((value - 1) * 10);
  };

  const handleFilter = (values) => {
    setJobType(values.jobType);
    if (values.jobCategory) {
      const arrJobCategory = [];
      arrJobCategory.push(values.jobCategory);
      setJobCategory(arrJobCategory);
    }
  };

  const handleReset = () => {
    const id = localStorage.getItem('user_id');
    fetchPost(id, 'JOB', offset);
    resetFilter();
    setJobCategory([]);
    setJobType();
  };

  return (
    <div>
      <PostList
        handleReset={handleReset}
        handleFilter={handleFilter}
        jobCategory={jobCategory}
      >
        {showPostItem(posts.data)}
      </PostList>
      <div className={classes.pagination}>
        <Pagination
          count={Math.ceil(posts.total / 10)}
          onChange={handleChangePage}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
    jobCategory: state.jobCategory,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPost: (idCompany, postType, offset, jobType, jobCategory) => {
      dispatch(
        fetchPostByidCompany(idCompany, postType, offset, jobType, jobCategory),
      );
    },
    deletePost: (postId) => {
      dispatch(deletePostItem(postId));
    },
    getJobCategory: () => {
      dispatch(fetchJobCategory());
    },
    resetFilter: () => {
      dispatch(reset('filterPost'));
    },
  };
};

PostListComponent.propTypes = {
  fetchPost: PropTypes.func.isRequired,
  posts: PropTypes.instanceOf(Object).isRequired,
  deletePost: PropTypes.func.isRequired,
  classes: PropTypes.instanceOf(Object).isRequired,
  jobCategory: PropTypes.instanceOf(Array).isRequired,
  getJobCategory: PropTypes.func.isRequired,
  resetFilter: PropTypes.func.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
)(PostListComponent);
