/* eslint-disable react/destructuring-assignment */
import React from 'react';
import Table from 'react-bootstrap/Table';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faNewspaper } from '@fortawesome/free-solid-svg-icons';
import 'react-datepicker/dist/react-datepicker.css';
import PropTypes from 'prop-types';
import Filter from './Filter';

const PostList = (props) => {
  const { jobCategory, handleFilter, handleReset, children } = props;
  return (
    <div className="post-list">
      <Row>
        <Col lg="12">
          <Row style={{ padding: '15px 15px' }}>
            <Col lg="1" sm="2">
              <div className="icon-header">
                <FontAwesomeIcon icon={faNewspaper} />
              </div>
            </Col>
            <Col lg="11" sm="10">
              <div className="title text-left" style={{ padding: '0px' }}>
                <h3>DANH SÁCH TIN TUYỂN DỤNG</h3>

                <h6 style={{ color: 'gray' }}>
                  (Danh sách những bài tin tìm việc nhà tuyển dụng đã đăng)
                </h6>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg="12">
          <Filter
            handleReset={handleReset}
            onSubmit={handleFilter}
            jobCategory={jobCategory}
          />
        </Col>
        <Col lg="12" style={{ marginTop: '20px' }}>
          <Table className="table-post" responsive="lg" bordered hover>
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên bài đăng</th>
                <th>Mức lương</th>
                <th>Loại công việc</th>
                <th>Số lượng</th>
                <th>Ngày đăng</th>
                <th>Trạng thái</th>
                <th>Thao tác</th>
              </tr>
            </thead>
            <tbody>{children}</tbody>
          </Table>
        </Col>
      </Row>
    </div>
  );
};

PostList.propTypes = {
  children: PropTypes.instanceOf(Object).isRequired,
  jobCategory: PropTypes.instanceOf(Array).isRequired,
  handleFilter: PropTypes.func.isRequired,
  handleReset: PropTypes.func.isRequired,
};

export default PostList;
