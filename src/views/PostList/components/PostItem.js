import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Dropdown from 'react-bootstrap/Dropdown';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import { Link } from 'react-router-dom';
import PostDetail from '../../PostDetail';
import DeleteConfirm from '../../../Components/Dashboard/DeleteConfirm';
import { formatter } from '../../../Helpers/ToCurrency';

const styles = {
  btn: {
    marginRight: 44,
  },
  jobCategory: {
    '& div': {
      margin: '0px 4px 4px 0px',
    },
  },
  detail: {
    '& a': {
      textDecoration: 'none',
    },
  },
};

class PostItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenDelete: false,
      isOpenDetail: false,
    };
  }

  handleClose = (status) => {
    if (status === 'delete') {
      this.setState({
        isOpenDelete: false,
      });
    } else {
      this.setState({
        isOpenDetail: false,
      });
    }
  };

  handleOpen = (status) => {
    if (status === 'delete') {
      this.setState({
        isOpenDelete: true,
      });
    } else {
      this.setState({
        isOpenDetail: true,
      });
    }
  };

  render() {
    const { post, onDelete, index, classes } = this.props;
    const { isOpenDelete, isOpenDetail } = this.state;
    const createdTime = new Date(_.get(post, 'createdTime', ''));
    const { jobCategory } = post;
    return (
      <>
        <tr>
          <td>{index}</td>
          <td>{_.get(post, 'name', '')}</td>
          <td>{post.salary ? formatter.format(post.salary) : 'Thỏa thuận'}</td>
          <td className={classes.jobCategory}>
            {jobCategory &&
              jobCategory.map((cat) => <Chip label={cat} key={cat} />)}
          </td>
          <td>{_.get(post, 'amount', '')}</td>
          <td>{createdTime.toLocaleDateString()}</td>
          <td>
            {post.isAcceptedByAdmin ? (
              <Chip label="Đã duyệt" color="primary" />
            ) : (
              <Chip label="Chờ duyệt" color="secondary" />
            )}
          </td>
          <td>
            <div className="text-center">
              <Dropdown>
                <Dropdown.Toggle variant="danger" id="dropdown-basic">
                  <FormatListBulletedIcon />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item className={classes.detail} href="">
                    <Link
                      to={{
                        pathname: '/post-detail',
                        aboutProps: {
                          post,
                          isSeeDetail: true,
                        },
                      }}
                    >
                      Xem chi tiết
                    </Link>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href=""
                    onClick={() => this.handleOpen('delete')}
                  >
                    Xóa bài viết
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </td>
        </tr>
        <DeleteConfirm
          isOpenDelete={isOpenDelete}
          handleClose={this.handleClose}
          onDelete={onDelete}
          postId={post.postId}
        />
        <Dialog
          onClose={() => this.handleClose('detail')}
          aria-labelledby="customized-dialog-title"
          open={isOpenDetail}
        >
          <DialogContent dividers>
            <PostDetail post={post} />
          </DialogContent>
          <DialogActions>
            <Button
              className={classes.btn}
              autoFocus
              onClick={() => this.handleClose('detail')}
              color="primary"
            >
              Trở lại
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}
PostItem.propTypes = {
  post: PropTypes.instanceOf(Object).isRequired,
  onDelete: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(PostItem);
