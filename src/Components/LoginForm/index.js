/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/require-default-props */
/* eslint-disable import/no-mutable-exports */
import React from 'react';
import { reduxForm, Field } from 'redux-form';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Alert from '@material-ui/lab/Alert';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import TextField from '../CustomField/TextField';
import CopyRight from '../CopyRight';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  remember: {
    marginTop: 10,
  },
  register: {
    margin: 'auto',
  },
  forgetPass: {
    color: '#007bff',
    textDecoration: 'none',
    backgroundColor: 'transparent',
    '&:hover': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  },
}));

let LoginForm = (props) => {
  const classes = useStyles();
  const { handleSubmit, user, handleClickOpen } = props;
  const loggedIn = _.get(user, 'loggedIn', null);
  return (
    <Container component="main" maxWidth="xs">
      {/* Check login failure => alert  */}
      {loggedIn === false && (
        <Alert variant="filled" severity="error">
          Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu
        </Alert>
      )}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Đăng nhập
        </Typography>
        <form onSubmit={handleSubmit} className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Field
                name="email"
                component={TextField}
                label="Nhập Username"
                variant="outlined"
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="password"
                component={TextField}
                fullWidth
                label="Nhập mật khẩu"
                variant="outlined"
                required
                type="password"
              />
            </Grid>
            <Grid item xs={5}>
              <span onClick={handleClickOpen} className={classes.forgetPass}>
                Quên mật khẩu?
              </span>
            </Grid>
            <Grid item xs={7} className={classes.register}>
              <Link to="/register">Bạn chưa có tài khoản? Đăng ký ngay.</Link>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Đăng nhập
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <CopyRight />
      </Box>
    </Container>
  );
};

LoginForm = reduxForm({
  form: 'loginForm',
})(LoginForm);

LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
  handleClickOpen: PropTypes.func,
  user: PropTypes.instanceOf(Object),
};

export default LoginForm;
