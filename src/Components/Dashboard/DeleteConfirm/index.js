import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "react-bootstrap/Button";
import PropTypes from "prop-types";

let DeleteConfirm = (props) => {
  const { isOpenDelete, handleClose, onDelete, postId } = props;
  return (
    <Dialog
      open={isOpenDelete}
      onClose={() => handleClose("delete")}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Bạn có thật sự muốn xóa bài đăng này?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleClose("delete")} variant="primary">
          Trở lại
        </Button>
        <Button
          onClick={() => {
            setTimeout(() => { handleClose("delele") }, 1000)
            return onDelete(postId);
          }}
          variant="danger"
        >
          Xóa
        </Button>
      </DialogActions>
    </Dialog>
  );
};

DeleteConfirm.propTypes = {
  isOpenDelete: PropTypes.bool,
  handleClose: PropTypes.func,
  onDelete: PropTypes.func,
  postId: PropTypes.string
};

export default DeleteConfirm;
