/* eslint-disable react/no-array-index-key */
/* eslint-disable no-dupe-class-members */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import FileBase64 from 'react-file-base64';
import EditIcon from '@material-ui/icons/Edit';
import Form from 'react-bootstrap/Form';
import Select2 from 'react-select2-wrapper';
import 'react-select2-wrapper/css/select2.css';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';

import draftToHtml from 'draftjs-to-html';

class BlogForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgFiles: [],
      editorState: EditorState.createEmpty(),
    };
  }

  getFiles(files) {
    this.setState({ imgFiles: files });
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
    });
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent()),
    );
  }

  getFiles(files) {
    this.setState({ imgFiles: files });
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
    });
    const htmlContent = draftToHtml(
      convertToRaw(editorState.getCurrentContent()),
    );
  }

  render() {
    const { editorState } = this.state;
    return (
      <Container className="blog-component">
        <Row>
          <Col lg="12 text-center blog-header">
            <h2>
              <EditIcon /> Form Đăng Blog{' '}
            </h2>
            <h6>
              (Trang giúp bạn chia sẻ những kinh nghiệm thông qua những bài
              blog)
            </h6>
            {/* <FileBase64
                                className="input-img"
                                multiple={true}
                                onDone={this.getFiles.bind(this)} /> */}
          </Col>
          <Col className="blog-body" lg="12" style={{ marginTop: '30px' }}>
            <Row>
              <Col lg={{ span: 8, offset: 2 }} className="form-blog">
                <Form>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridBlogName">
                      <Form.Label>Tên Blog</Form.Label>
                      <Form.Control type="text" placeholder="Nhập tên blog" />
                    </Form.Group>
                  </Form.Row>

                  <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={12}>
                      Chuyên mục
                    </Form.Label>
                    <Col sm={12}>
                      <Select2
                        multiple
                        defaultValue={[1]}
                        data={[
                          { text: 'Công nghệ', id: 1 },
                          { text: 'Tin học', id: 2 },
                          { text: 'Kinh tế', id: 3 },
                          { text: 'Nhân sự', id: 4 },
                        ]}
                        onOpen={this.cbOpen}
                        onClose={this.cbClose}
                        onSelect={this.cbSelect}
                        onChange={this.cbChange}
                        onUnselect={this.cbUnselect}
                        options={{
                          placeholder: 'Chọn địa điểm',
                        }}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={12}>
                      Tag blog
                    </Form.Label>
                    <Col sm={12}>
                      <Select2
                        multiple
                        defaultValue={[1]}
                        data={[
                          { text: 'Công nghệ', id: 1 },
                          { text: 'Tin học', id: 2 },
                          { text: 'Kinh tế', id: 3 },
                          { text: 'Nhân sự', id: 4 },
                        ]}
                        onOpen={this.cbOpen}
                        onClose={this.cbClose}
                        onSelect={this.cbSelect}
                        onChange={this.cbChange}
                        onUnselect={this.cbUnselect}
                        options={{
                          placeholder: 'Chọn địa điểm',
                        }}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={12}>
                      Ảnh đại diện
                    </Form.Label>
                    <Col sm={12}>
                      <FileBase64
                        className="input-img"
                        multiple
                        onDone={this.getFiles.bind(this)}
                      />
                    </Col>
                    <Col sm={12}>
                      {this.state.imgFiles.map((file, i) => {
                        return (
                          <img
                            style={{ width: '100%' }}
                            className="img-file"
                            key={i}
                            src={file.base64}
                          />
                        );
                      })}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={12}>
                      Nội dung bài blog
                    </Form.Label>
                    <Col sm={12}>
                      <Editor
                        editorState={editorState}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        onEditorStateChange={this.onEditorStateChange.bind(
                          this,
                        )}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Col sm={12} className="text-center">
                      <Button variant="info">Đăng bài</Button>
                    </Col>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default BlogForm;
