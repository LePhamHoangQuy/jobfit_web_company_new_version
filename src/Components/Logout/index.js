/* eslint-disable camelcase */
export const Logout = () => {
  const user_id = localStorage.getItem('user_id');
  if (user_id) {
    localStorage.clear();
    window.location.href = '/login';
  }
};
