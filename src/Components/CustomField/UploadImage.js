/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { uploadAvatar } from '../../Actions/profile.actions';

class FieldFileInput extends Component {
  onChange = async (e) => {
    const {
      input: { onChange },
    } = this.props;
    const image = e.target.files[0];
    const formData = new FormData();
    formData.append('image', image, image.name);
    this.props.updateAvatar(formData);
    onChange(e);
  };

  render() {
    const {
      input: { value },
    } = this.props;
    const { input, label } = this.props;
    return (
      <div className="input-field">
        <label>{label}</label>
        <div>
          <input
            type="file"
            accept=".jpg, .png, .jpeg"
            onChange={this.onChange}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateAvatar: (file) => {
      dispatch(uploadAvatar(file));
    },
  };
};

export default connect(null, mapDispatchToProps)(FieldFileInput);
