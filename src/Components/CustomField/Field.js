/* eslint-disable react/prop-types */
import React from 'react';
import TextField from '@material-ui/core/TextField';

export const renderTextField = ({
  label,
  input,
  fullWidth,
  multiline,
  rowsMax,
  InputLabelProps,
  variant,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    placeholder={label}
    rowsMax={rowsMax}
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
    fullWidth={fullWidth}
    multiline={multiline}
    InputLabelProps={InputLabelProps}
    variant={variant}
  />
);
