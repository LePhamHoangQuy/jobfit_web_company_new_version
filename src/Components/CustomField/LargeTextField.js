/* eslint-disable react/prop-types */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = {};

const renderLargeTextField = ({
  classes,
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => {
  return (
    <>
      <TextField
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
        variant="outlined"
        fullWidth
        multiline
      />
    </>
  );
};

export default withStyles(styles)(renderLargeTextField);
