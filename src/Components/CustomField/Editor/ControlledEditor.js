/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import {
  EditorState,
  convertToRaw,
  ContentState,
  convertFromHTML,
} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import { unemojify } from 'node-emoji';
import PropTypes from 'prop-types';

export default class ControlledEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      isInitValue: false,
    };

    this.props.onChange(
      draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())),
    );
  }

  componentDidUpdate() {
    if (this.state.isInitValue === false && this.props.value.length > 10) {
      this.setState({
        editorState: EditorState.createWithContent(
          ContentState.createFromBlockArray(convertFromHTML(this.props.value)),
        ),
        isInitValue: true,
      });
    }
  }

  onEditorStateChange = (editorState) => {
    const { onChange, value } = this.props;

    const newValue = unemojify(
      draftToHtml(convertToRaw(editorState.getCurrentContent())),
    );

    if (value !== newValue) {
      onChange(newValue);
    }

    this.setState({
      editorState,
    });
  };

  render() {
    const { editorState } = this.state;

    return (
      <div>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          toolbar={{
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: true },
          }}
        />
      </div>
    );
  }
}

ControlledEditor.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.any,
};
