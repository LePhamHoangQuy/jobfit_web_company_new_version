/* eslint-disable react/require-default-props */
/* eslint-disable import/no-mutable-exports */
import React from 'react';
import { reduxForm, Field } from 'redux-form';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import Alert from '@material-ui/lab/Alert';
import _ from 'lodash';
import TextField from '../CustomField/TextField';
import CopyRight from '../CopyRight';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

let RegisterForm = (props) => {
  const classes = useStyles();
  const { handleSubmit, registration } = props;
  const registering = _.get(registration, 'registering', null);
  return (
    <Container component="main" maxWidth="xs">
      {/* Check registerign => alert  */}
      {registering === true && (
        <Alert variant="filled" severity="success">
          Vui lòng kiểm tra email và xác thực để đăng ký.
        </Alert>
      )}
      {registering === false && (
        <Alert variant="filled" severity="error">
          Đăng ký thất bại, vui lòng kiểm tra lại.
        </Alert>
      )}

      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Đăng ký
        </Typography>
        <form onSubmit={handleSubmit} className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Field
                name="username"
                component={TextField}
                label="Nhập tên tài khoản"
                variant="outlined"
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="email"
                component={TextField}
                label="Nhập Email"
                required
                variant="outlined"
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="password"
                component={TextField}
                label="Nhập mật khẩu"
                required
                type="password"
                variant="outlined"
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="passwordConfirm"
                component={TextField}
                label="Nhập lại mật khẩu một lần nữa"
                required
                type="password"
                variant="outlined"
                fullWidth
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Đăng ký
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/login">Bạn đã có tài khoản? Đăng nhập</Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <CopyRight />
      </Box>
    </Container>
  );
};

RegisterForm = reduxForm({
  form: 'registerForm',
})(RegisterForm);

RegisterForm.propTypes = {
  handleSubmit: PropTypes.func,
  registration: PropTypes.instanceOf(Object).isRequired,
};

export default RegisterForm;
