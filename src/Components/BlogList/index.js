/* eslint-disable react/require-default-props */
import React from 'react';
import Table from 'react-bootstrap/Table';
import { Row, Col } from 'react-bootstrap';
import 'react-select2-wrapper/css/select2.css';
import 'react-datepicker/dist/react-datepicker.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBlog } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

const BlogList = ({ children }) => {
  return (
    <div className="blog-list">
      <Row>
        <Col lg="12">
          <Row style={{ padding: '15px 15px' }}>
            <Col lg="1">
              <div className="icon-header">
                <FontAwesomeIcon icon={faBlog} />
              </div>
            </Col>
            <Col lg="11">
              <div className="title text-left" style={{ padding: '0px' }}>
                <h3>DANH SÁCH BÀI BLOG</h3>

                <h6 style={{ color: 'gray' }}>
                  (Danh sách những bài blog mà công ty đã chia sẻ)
                </h6>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg="12" style={{ marginTop: '20px' }}>
          <Table className="table-post" responsive="lg" bordered hover>
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên bài blog</th>
                <th>Ngày đăng</th>
                <th>Tag blog</th>
                <th>Chuyên mục</th>
                <th>Trạng thái</th>
                <th>Thao tác</th>
              </tr>
            </thead>
            <tbody>{children}</tbody>
          </Table>
        </Col>
      </Row>
    </div>
  );
};

BlogList.propTypes = {
  children: PropTypes.instanceOf(Object),
};

export default BlogList;
