/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Chip from '@material-ui/core/Chip';
import DeleteConfirm from '../Dashboard/DeleteConfirm';

class BlogItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenDelete: false,
    };
  }

  handleClose = (status) => {
    if (status === 'delete') {
      this.setState({
        isOpenDelete: false,
      });
    }
  };

  handleOpen = (status) => {
    if (status === 'delete') {
      this.setState({
        isOpenDelete: true,
      });
    }
  };

  render() {
    const { post, index, onDelete } = this.props;
    const { isOpenDelete } = this.state;
    const createdTime = new Date(_.get(post, 'createdTime', ''));
    const jobCategory = _.get(post, 'jobCategory');
    const tags = _.get(post, 'tags');

    return (
      <>
        <tr>
          <td>{index}</td>
          <td> {_.get(post, 'name', '')} </td>
          <td>{createdTime.toLocaleDateString()} </td>
          <td>
            {!_.isEmpty(tags) &&
              post.tags.map((tag) => (
                <span className="blog-tag" key={tag}>
                  {tag}
                </span>
              ))}
          </td>
          <td>
            {!_.isEmpty(jobCategory) &&
              jobCategory.map((category) => (
                <span className="category-tag" key={category}>
                  {category}
                </span>
              ))}
          </td>
          <td>
            {post.isAcceptedByAdmin ? (
              <Chip label="Đã duyệt" color="primary" />
            ) : (
              <Chip label="Chờ duyệt" color="secondary" />
            )}
          </td>
          <td>
            <Dropdown>
              <Dropdown.Toggle variant="danger" id="dropdown-basic">
                <FormatListBulletedIcon />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link
                    to={{
                      pathname: '/blog-detail',
                      aboutProps: {
                        post,
                        isSeeDetail: true,
                      },
                    }}
                  >
                    Xem chi tiết
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item onClick={() => this.handleOpen('delete')}>
                  Xóa bài blog
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </td>
        </tr>
        <DeleteConfirm
          isOpenDelete={isOpenDelete}
          postId={post.postId}
          handleClose={this.handleClose}
          onDelete={onDelete}
        />
      </>
    );
  }
}

BlogItem.propTypes = {
  post: PropTypes.instanceOf(Object),
  index: PropTypes.number,
  onDelete: PropTypes.func,
};

export default BlogItem;
