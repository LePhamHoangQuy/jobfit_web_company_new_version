/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Logout } from '../Logout';

const styles = {
  navBar: {
    backgroundColor: '#2E86DE',
    minHeight: '5rem',
    color: 'white',
  },
  logo: {
    display: 'inline-block',
    margin: '22px 0 0 20px',
    fontSize: 24,
  },
  register: {
    background: '#00d600',
    padding: '8px 25px',
  },
  mainNavBar: {
    display: 'inline-block',
    float: 'right',
    margin: 22,
    '& a': {
      marginRight: 36,
      color: 'white',
      textDecoration: 'none',
    },
    '& :hover': {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
  },
};

const NavBar = (props) => {
  const { classes } = props;
  const username = localStorage.getItem('username');
  return (
    <nav className={classes.navBar}>
      <div className={classes.logo}>
        <a>JOBFIT</a>
      </div>
      <div className={classes.mainNavBar}>
        <Link to="#about">Giới thiệu</Link>
        <Link to="#partner">Nhà tuyển dụng</Link>
        <Link to="#dowloadApp">Tải App</Link>
        {username ? (
          <>
            <Link to="/profile">{`Xin chào, ${username}`}</Link>
            <Link onClick={() => Logout()} to="#">
              Đăng xuất
            </Link>
          </>
        ) : (
          <>
            <Link to="/login">Đăng nhập</Link>
            <Link to="/register" className={classes.register}>
              Đăng ký
            </Link>
          </>
        )}
      </div>
    </nav>
  );
};

NavBar.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(NavBar);
