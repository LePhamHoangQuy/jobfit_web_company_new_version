import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Jobfit from '../../assets/images/jobfit.png';

const styles = {
  nav: {
    background: `linear-gradient(90deg, rgba(9, 31, 38, 1) 0%, rgba(60, 130, 147, 1) 43%, rgba(76, 103, 159, 1) 67%, rgba(57, 96, 125, 1) 100%, rgba(43, 136, 136, 1) 100%, rgba(84, 64, 24, 1) 100%, rgba(0, 212, 255, 1) 100%, rgba(42, 49, 224, 1) 100%)`,
  },
  logo: {
    textAlign: 'center',
    padding: '10px',
  },
  logoImg: {
    width: '8%',
  },
};

const NavBar = (props) => {
  const { classes } = props;
  return (
    <div className={classes.nav}>
      <div className={classes.logo}>
        <Link to="/">
          <img className={classes.logoImg} alt="Logo" src={Jobfit} />
        </Link>
      </div>
    </div>
  );
};

NavBar.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default withStyles(styles)(NavBar);
