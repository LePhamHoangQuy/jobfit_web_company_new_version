// import socketIOClient from 'socket.io-client';
import SocketIO from 'socket.io-client';

const ENDPOINT = 'http://34.87.188.149:80';
// const ENDPOINT = 'http://localhost:5006';

export const socket = SocketIO(ENDPOINT, { transports: ['websocket'] });
