import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

import RouteWithLayout from '../Components/RouteWithLayout';
import { Main as MainLayout, Minimal as MinimalLayout } from '../layouts';
import { LoginRoute } from '../Components/PrivateRoute';
import NotFound from '../views/NotFound';
import Login from '../views/Login';
import Home from '../views/Home';
import Register from '../views/RegisterPage';
import Profile from '../views/Profile';
import PostList from '../views/PostList';
import PostDetail from '../views/PostDetail';
import Post from '../views/Post';
import BlogList from '../views/BlogList';
import CandidateList from '../views/CandidateList';
import Blog from '../views/Blog';
// import { routesMain } from './Routes';
import CandidateDetail from '../views/CandidateDetail';
import CandidateFollow from '../views/CandidateFollow';
import BlogDetail from '../views/BlogDetail';
import UpdatePassword from '../views/UpdatePassword';

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/register" exact component={Register} />
      <LoginRoute path="/login" component={Login} exact />
      {/* {routesMain.map((route) => (
        <RouteWithLayout
          key={route.path}
          component={route.component}
          exact={route.exact}
          layout={route.layout}
          path={route.path}
        />
      ))} */}
      <RouteWithLayout
        component={Profile}
        exact
        layout={MainLayout}
        path="/profile"
      />
      <RouteWithLayout
        component={PostList}
        exact
        layout={MainLayout}
        path="/post-list"
      />
      <RouteWithLayout
        component={PostDetail}
        exact
        layout={MainLayout}
        path="/post-detail"
      />
      <RouteWithLayout
        component={Post}
        exact
        layout={MainLayout}
        path="/post-job"
      />
      <RouteWithLayout
        component={BlogList}
        exact
        layout={MainLayout}
        path="/blog-list"
      />
      <RouteWithLayout
        component={Blog}
        exact
        layout={MainLayout}
        path="/post-blog"
      />
      <RouteWithLayout
        component={CandidateList}
        exact
        layout={MainLayout}
        path="/candidate-list"
      />
      <RouteWithLayout
        component={CandidateDetail}
        exact
        layout={MainLayout}
        path="/candidate-detail"
      />
      <RouteWithLayout
        component={CandidateFollow}
        exact
        layout={MainLayout}
        path="/candidate-follow"
      />
      <RouteWithLayout
        component={BlogDetail}
        exact
        layout={MainLayout}
        path="/blog-detail"
      />
      <RouteWithLayout
        component={UpdatePassword}
        exact
        layout={MainLayout}
        path="/update-password"
      />
      <RouteWithLayout
        component={NotFound}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
